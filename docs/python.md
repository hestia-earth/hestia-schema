# HESTIA Schema

## License

This project is in the worldwide public domain, released under the [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

![Public Domain Dedication](https://licensebuttons.net/p/zero/1.0/88x31.png)

## Install

```bash
pip install hestia_earth.schema
```

### Usage

```python
from hestia_earth.schema import Cycle

cycle = Cycle()
cycle.fields.name = 'New Cycle'
```
