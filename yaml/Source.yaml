class:
  name: Source
  type: Node
  examples:
    - source.jsonld
  doc: The Source of these data.

  properties:
    - name: name
      type: string
      doc: The name from <code>[bibliography](/schema/Source#bibliography)</code>.
      extend: true
      internal: true
      required: true
      searchable: true

    - name: bibliography
      type: Embed[Bibliography]
      doc: The bibliographic information describing the document or dataset.
      required: true
      searchable: true
      unique: true
      exportDefaultCSV: true

    - name: metaAnalyses
      type: List[Ref[Source]]
      doc: >
        If multiple Sources were consolidated by meta-analysis, a list of sources
        describing the meta-analysis document or dataset.
      searchable: true
      exportDefaultCSV: true

    - name: uploadBy
      type: Ref[Actor]
      doc: The user who uploaded these data.
      internal: true
      required: true

    - name: uploadNotes
      type: string
      doc: >
        A free text field to describe the data upload, including any issues in the original data,
        assumptions made, or other important points.

    - name: validationDate
      type: date
      format: date
      doc: >
        The date the data was checked by an independent data validator
        [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      internal: true

    - name: validationBy
      type: List[Ref[Actor]]
      doc: The people/organizations who/which validated these data.
      internal: true

    - name: intendedApplication
      type: string
      doc: >
        The intended application (see
        [ISO 14044:2006](https://www.iso.org/standard/38498.html)).

    - name: studyReasons
      type: string
      doc: >
        The reasons for carrying out the study (see
        [ISO 14044:2006](https://www.iso.org/standard/38498.html)).

    - name: intendedAudience
      type: string
      doc: >
        The intended audience i.e., to whom the results of the study are intended to
        be communicated (see [ISO 14044:2006](https://www.iso.org/standard/38498.html)).

    - name: comparativeAssertions
      type: boolean
      doc: >
        Whether the results are intended to be used in comparative assertions
        (see [ISO 14044:2006](https://www.iso.org/standard/38498.html)).

    - name: sampleDesign
      type: Ref[Term]
      doc: The sample design, taken from the [Glossary](/glossary?termType=sampleDesign).
      searchable: true
      exportDefaultCSV: true

    - name: weightingMethod
      type: string
      doc: For sample designs involving weights, a description of the weights.
      searchable: true
      exportDefaultCSV: true

    - name: experimentDesign
      type: Ref[Term]
      doc: >
        The design of the experiment, taken from the
        [Glossary](/glossary?termType=experimentDesign).
      searchable: true
      exportDefaultCSV: true

    - name: originalLicense
      type: string
      enum:
        - CC0
        - CC-BY
        - CC-BY-SA
        - CC-BY-NC
        - CC-BY-NC-SA
        - CC-BY-NC-ND
        - CC-BY-ND
        - GNU-FDL
        - other public license
        - no public license
      doc: >
        The [public copyright license](https://en.wikipedia.org/wiki/Public_copyright_license) that
        the original version of these data were licensed under.
        Required for public uploads only.
        <ul class="is-pl-3 is-list-style-disc">
          <li>
            <code>CC0</code> is a [Creative Commons license](https://creativecommons.org/share-your-work/public-domain/cc0/)
            that relinquishes copyright and releases the material into the public domain.
          </li>
          <li>
            <code>CC-BY</code> is a [Creative Commons license](https://creativecommons.org/licenses/by/4.0/)
            allowing reuse as long as credit is given to the author of the material.
          </li>
          <li>
            <code>CC-BY-SA</code> is a [Creative Commons license](https://creativecommons.org/licenses/by-sa/4.0/)
            allowing reuse as long as credit is given to the author of the material and adaptations
            of the material are shared under the same license terms.
          </li>
          <li>
            <code>CC-BY-NC</code> is a [Creative Commons license](https://creativecommons.org/licenses/by-nc/4.0/)
            allowing reuse for non-commercial purposes as long as credit is given to the author of the material.
          </li>
          <li>
            <code>CC-BY-NC-SA</code> is a [Creative Commons license](https://creativecommons.org/licenses/by-nc-sa/4.0/)
            allowing reuse for non-commercial purposes as long as credit is given to the author of the material
            and adaptations of the material are shared under the same license terms.
          </li>
          <li>
            <code>CC-BY-NC-ND</code> is a [Creative Commons license](https://creativecommons.org/licenses/by-nc-nd/4.0/)
            allowing reuse for non-commercial purposes as long as credit is given to the author of the material
            but preventing derivatives or adaptations of the material being made.
          </li>
          <li>
            <code>CC-BY-ND</code> is a [Creative Commons license](https://creativecommons.org/licenses/by-nc-nd/4.0/)
            allowing reuse as long as credit is given to the author of the material
            but preventing derivatives or adaptations of the material being made.
          </li>
          <li>
            <code>GNU-FDL</code> is a [GNU Project license](https://www.gnu.org/licenses/fdl-1.3.html)
            allowing reuse as long as adaptations of the material are shared under the same license terms.
          </li>
          <li>
            <code>other public license</code> means the material is licensed under a license not listed above.
          </li>
          <li>
            <code>no public license</code> means the material is under copyright by the original author or
            publisher with no rights granted for public use beyond the exceptions to copyright in national law.
          </li>
        </ul>
      searchable: true
      exportDefaultCSV: true
      recommended: true

    - name: dataPrivate
      type: boolean
      doc: >
        If these data are private. Private means that HESTIA administrators can access these data and
        you can grant access to other platform users, but these data will not be made available to
        any other users of the platform or distributed to third parties.
        If we find the Source on the [Mendeley](https://www.mendeley.com/) catalogue,
        the data in the Source node are public and we set this field to <code>false</code>.
      required: true
      default: false

    - name: originalId
      type: string
      doc: >
        The identifier for these data in the source database (e.g., if the data were converted
        from openLCA or ecoinvent, the id field from that database).
      internal: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true
      searchable: true

    - name: createdAt
      type: date
      format: date
      doc: >
        Date created on HESTIA in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      internal: true
      searchable: true

    - name: updatedAt
      type: date
      format: date
      doc: >
        Last update date on HESTIA in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      internal: true
      searchable: true

  validation:
    allOf:
      - if:
          required:
            - sampleDesign
        then:
          properties:
            sampleDesign:
              properties:
                termType:
                  enum:
                    - sampleDesign
      - if:
          required:
            - experimentDesign
        then:
          properties:
            experimentDesign:
              properties:
                termType:
                  enum:
                    - experimentDesign
      - if:
          required:
            - dataPrivate
          properties:
            dataPrivate:
              const: false
        then:
          required:
            - originalLicense
