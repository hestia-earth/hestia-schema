class:
  name: Product
  type: Blank Node
  examples:
    - product.jsonld
  doc: >
    A Product created during a [Cycle](/schema/Cycle) or [Transformation](/schema/Transformation).
    Each Product must be unique, and the fields which
    determine uniqueness are defined in the <code>[products](/schema/Cycle#products)</code>
    field of the Cycle or [Transformation](/schema/Transformation#products).

  properties:
    - name: term
      type: Ref[Term]
      doc: >
        A reference to the [Term](/schema/Term) describing the Product.
      required: true
      searchable: true
      exportDefaultCSV: true

    - name: description
      type: string
      doc: A short description of the Product.
      exportDefaultCSV: true

    - name: variety
      type: string
      doc: >
        The variety (cultivar) of a crop or breed of animal. Standardised variety names are
        defined in external glossaries, such as the [OECD](https://www.niab.com/oecdv2/variety),
        [GEVES](https://www.geves.fr/catalogue-france/), [PLUTO](https://www.upov.int/pluto/en/),
        or [CPVO](https://online.plantvarieties.eu/) glossaries.
      exportDefaultCSV: true

    - name: value
      type: array[number|null]
      minimum: 0
      doc: >
        The quantity of the Product. If an average, it should always be the mean.
        Can be a single number (array of length one) or an array of numbers
        with associated [dates](/schema/Product#dates) (e.g., for multiple harvests in one [Cycle]).
        The units are always specified in the [Term](/schema/Term).
        For crops, value should always be per harvest or per year, following
        [FAOSTAT conventions](http://www.fao.org/economic/the-statistics-division-ess/methodology/methodology-systems/crops-statistics-concepts-definitions-and-classifications/en/).
        Terms from the [land cover](/glossary?termType=landCover) glossary can also be added as
        Products to specify cover crops, and the units in this case are <code>% area</code>.
      exportDefaultCSV: true
      recommended: true
      searchable: true

    - name: sd
      type: array[number|null]
      minimum: 0
      doc: The standard deviation of <code>[value](/schema/Product#value)</code>.
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: min
      type: array[number|null]
      doc: The minimum of <code>[value](/schema/Product#value)</code>.
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: max
      type: array[number|null]
      doc: The maximum of <code>[value](/schema/Product#value)</code>.
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: statsDefinition
      type: string
      enum:
        - sites
        - cycles
        - replications
        - other observations
        - time
        - spatial
        - regions
        - simulated
        - modelled
      doc: >
        What the descriptive statistics (<code>[sd](/schema/Product#sd)</code>, <code>[min](/schema/Product#min)</code>,
        <code>[max](/schema/Product#max)</code>, and <code>[value](/schema/Product#value)</code>) are calculated across, or
        whether they are simulated or the output of a model. <code>spatial</code> refers to
        descriptive statistics calculated across spatial units (e.g., pixels) within a region
        or country. <code>time</code> refers to descriptive statistics calculated across units
        of time (e.g., hours).
      exportDefaultCSV: true

    - name: observations
      type: array[number|null]
      doc: >
        The number of observations the descriptive statistics are calculated over.
      searchable: true
      minimum: 1
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: dates
      type: array[string]
      pattern: ^([0-9]{4}|-)(-[0-9]{2})?(-[0-9]{2})?([T][0-2][0-9]\:[0-5][0-9]\:[0-5][0-9](((\+|\-)[0-1][0-9]:[0-5][0-9])|Z)?)?$
      datePattern: true
      doc: >
        A corresponding array to [value](/schema/Product#value), representing the dates of the Products
        in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format
        (YYYY-MM-DD, YYYY-MM, YYYY, --MM-DD, --MM, or YYYY-MM-DDTHH:mm:ssZ).
      arraySameSize:
        - value
      exportDefaultCSV: true

    - name: startDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      datePattern: true
      doc: >
        For Products created over periods, the start date of the Product if different from the
        start date of the [Cycle](/schema/Cycle) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format
        (YYYY-MM-DD, YYYY-MM, or YYYY).
      exportDefaultCSV: true

    - name: endDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      datePattern: true
      doc: >
        For Products created over periods, the end date of the Product if different from the
        end date of the [Cycle](/schema/Cycle) in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format
        (YYYY-MM-DD, YYYY-MM, or YYYY).
      exportDefaultCSV: true

    - name: methodClassification
      type: string
      enum:
        - physical measurement
        - verified survey data
        - non-verified survey data
        - modelled
        - estimated with assumptions
        - consistent external sources
        - inconsistent external sources
        - expert opinion
        - unsourced assumption
      doc: >
        A classification of the method used to acquire or estimate the
        <code>[term](/schema/Product#term)</code> and <code>[value](/schema/Product#value)</code>. Overrides the
        <code>[defaultMethodClassification](/schema/Cycle#defaultMethodClassification)</code>
        specified in the [Cycle](/schema/Cycle). <code>methodClassification</code> should be specified
        separately for <code>[properties](/schema/Product#properties)</code>
        (see <code>[Property](/schema/Property#methodClassification)</code>) and
        <code>[transport](/schema/Product#transport)</code> (see
        <code>[Transport](/schema/Transport#methodClassification)</code>).
        <ul class="is-pl-3 is-list-style-disc">
          <li>
            <code>physical measurement</code> means the amount is quantified using weighing,
            volume measurement, metering, chemical methods, or other physical approaches.
          </li>
          <li>
            <code>verified survey data</code> means the data are initially collected through
            surveys; all or a subset of the data are verified using physical methods; and
            erroneous survey data are discarded or corrected.
          </li>
          <li>
            <code>non-verified survey data</code> means the data are collected through
            surveys that have not been subjected to verification.
          </li>
          <li>
            <code>modelled</code> means a previously calibrated model is used to estimate
            this data point from other data points describing this Cycle.
          </li>
          <li>
            <code>estimated with assumptions</code> means a documented assumption is used
            to estimate this data point from other data points describing this Cycle.
          </li>
          <li>
            <code>consistent external sources</code> means the data are taken from external
            datasets referring to different producers/enterprises:
            <ul class="is-pl-4 is-list-style-disc">
              <li>
                Using the same technology (defined as the same
                [System](/glossary?termType=system) or the same key [Practices](/schema/Practice)
                as those specified in the Cycle);
              </li>
              <li>
                At the same date (defined as occurring within the
                [startDate](/schema/Cycle#startDate) and [endDate](/schema/Cycle#endDate) of the Cycle);
                and
              </li>
              <li>
                In the same [region](/schema/Site#region) or [country](/schema/Site#country).
              </li>
            </ul>
            Modelling or assumptions may have also been used to transform these data.
          </li>
          <li>
            <code>inconsistent external sources</code> means the data are taken from external
            datasets referring to different producers/enterprises:
            <ul class="is-pl-4 is-list-style-disc">
              <li>
                Using a different technology (defined as a different
                [System](/glossary?termType=system) or using different key
                [Practices](/schema/Practice) to those specified in the Cycle);
              </li>
              <li>
                At a different date (defined as occurring within the
                [startDate](/schema/Cycle#startDate) and [endDate](/schema/Cycle#endDate) of the Cycle);
                or
              </li>
              <li>
                In a different [region](/schema/Site#region) or [country](/schema/Site#country).
              </li>
            </ul>
            Modelling or assumptions may have also been used to transform these data.
          </li>
          <li>
            <code>expert opinion</code> means the data have been estimated by experts in
            the field.
          </li>
          <li>
            <code>unsourced assumption</code> means the data do not have a clear source
            and/or are based on assumptions only.
          </li>
        </ul>
      searchable: true
      exportDefaultCSV: true

    - name: methodClassificationDescription
      type: string
      doc: >
        A justification of the <code>[methodClassification](/schema/Product#methodClassification)</code>
        used. If the data were <code>estimated with assumptions</code> this field should
        also describe the assumptions. This is a required field if
        <code>[methodClassification](/schema/Product#methodClassification)</code> is specified.
      exportDefaultCSV: true

    - name: model
      type: Ref[Term]
      doc: >
        A reference to the [Term](/schema/Term) describing the [model](/glossary?termType=model) used to
        estimate these data.
      searchable: true
      exportDefaultCSV: true

    - name: modelDescription
      type: string
      doc: A free text field, describing the model used to estimate these data.
      exportDefaultCSV: true

    - name: fate
      type: string
      enum:
        - sold
        - sold to export market
        - sold to domestic market
        - sold for breeding
        - sold for fattening
        - sold for slaughter
        - home consumption
        - fodder
        - bedding
        - breeding
        - saved for seeds
        - processing
        - burnt for fuel
        - burnt
        - anaerobic digestion
        - composted
        - used as fertiliser
        - used as soil amendment
        - used as mulch
      doc: >
        The fate of the Product. Use [Transformations](./Transformation) where possible to represent
        the conversion of one Product into another.
      exportDefaultCSV: true

    - name: price
      type: number
      minimum: 0
      doc: >
        The sale price of this Product. The price should be expressed per the units defined in
        the <code>[term](/schema/Product#term)</code>, for example per "kg liveweight".
        The <code>[currency](/schema/Product#currency)</code> must be specified.
      exportDefaultCSV: true

    - name: priceSd
      type: number
      minimum: 0
      doc: The standard deviation of <code>[price](/schema/Product#price)</code>.
      exportDefaultCSV: true

    - name: priceMin
      type: number
      minimum: 0
      doc: The minimum of <code>[price](/schema/Product#price)</code>.
      exportDefaultCSV: true

    - name: priceMax
      type: number
      minimum: 0
      doc: The maximum of <code>[price](/schema/Product#price)</code>.
      exportDefaultCSV: true

    - name: priceStatsDefinition
      type: string
      enum:
        - cycles
        - time
        - cycles and time
      doc: >
        What the descriptive statistics for <code>[price](/schema/Product#price)</code> are calculated
        across.
      exportDefaultCSV: true

    - name: revenue
      type: number
      minimum: 0
      doc: >
        The total revenue (<code>[price](/schema/Product#price) <code>x</code> [quantity](/schema/Product#value)</code>)
        of this Product. The <code>[currency](/schema/Product#currency)</code> must be specified.
      exportDefaultCSV: true

    - name: revenueSd
      type: number
      minimum: 0
      doc: The standard deviation of <code>[revenue](/schema/Product#revenue)</code>.
      exportDefaultCSV: true

    - name: revenueMin
      type: number
      minimum: 0
      doc: The minimum of <code>[revenue](/schema/Product#revenue)</code>.
      exportDefaultCSV: true

    - name: revenueMax
      type: number
      minimum: 0
      doc: The maximum of <code>[revenue](/schema/Product#revenue)</code>.
      exportDefaultCSV: true

    - name: revenueStatsDefinition
      type: string
      enum:
        - cycles
        - time
        - cycles and time
      doc: >
        What the descriptive statistics for <code>[revenue](/schema/Product#revenue)</code> are calculated
        across.
      exportDefaultCSV: true

    - name: currency
      type: string
      pattern: ^[A-Z]{3}$
      doc: >
        The three letter currency code in [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)
        format.
      exportDefaultCSV: true

    - name: economicValueShare
      type: number
      minimum: 0
      maximum: 100
      doc: >
        The economic value (typically revenue) of this Product, divided by the
        total economic value of all Products, expressed as a percentage.
      exportDefaultCSV: true

    - name: primary
      type: boolean
      doc: >
        Where the are multiple products, whether this product is the primary product.
        Defaults to true if there is only one product or if
        <code>[economicValueShare](/schema/Product#economicValueShare) > 50</code>.
      searchable: true
      exportDefaultCSV: true

    - name: source
      type: Ref[Source]
      doc: >
        A reference to the [Source](/schema/Source) of these data, if different from the
        <code>[defaultSource](/schema/Cycle#defaultSource)</code> of the [Cycle](/schema/Cycle).
      exportDefaultCSV: true

    - name: properties
      type: List[Property]
      doc: >
        A list of [Properties](/schema/Property) of the Product, which would override any
        default properties specified in the <code>[term](/schema/Product#term)</code>. For crops, dry matter is
        a default property of the [Term](/schema/Term) and can be changed by adding
        [dry matter](/term/dryMatter) here.
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - key.@id
        - value
        - share
        - date
        - startDate
        - endDate

    - name: transport
      type: List[Transport]
      doc: >
        A list of [Transport](/schema/Transport) stages to take this Product to the
        final location within the [Site](/schema/Site). For example, the Transport
        required to take harvested crops from the field to the barn where they are stored
        before being sold.
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - value
        - distance

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true

    - name: aggregated
      type: array[string]
      doc: A list of fields that have been 'aggregated' using data from multiple [Cycles](/schema/Cycle).
      internal: true

    - name: aggregatedVersion
      type: array[string]
      doc: A list of versions of the aggregation engine corresponding to each aggregated field.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - term
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - animalProduct
                    - crop
                    - cropResidue
                    - electricity
                    - feedFoodAdditive
                    - forage
                    - fuel
                    - landCover
                    - liveAnimal
                    - liveAquaticSpecies
                    - excreta
                    - organicFertiliser
                    - inorganicFertiliser
                    - otherOrganicChemical
                    - otherInorganicChemical
                    - processingAid
                    - processedFood
                    - seed
                    - soilAmendment
                    - substrate
                    - material
                    - waste
      - if:
          required:
            - model
        then:
          properties:
            model:
              properties:
                termType:
                  enum:
                    - model
      - if:
          anyOf:
            - required:
                - min
            - required:
                - max
            - required:
                - sd
        then:
          required:
            - statsDefinition
      - if:
          required:
            - statsDefinition
        then:
          anyOf:
            - required:
                - value
            - required:
                - min
            - required:
                - max
            - required:
                - sd
      - if:
          anyOf:
            - required:
                - revenue
            - required:
                - price
        then:
          required:
            - currency
      - if:
          required:
            - methodClassification
        then:
          required:
            - methodClassificationDescription
      - if:
          anyOf:
            - required:
              - priceMin
            - required:
              - priceMax
            - required:
              - priceSd
        then:
          required:
            - priceStatsDefinition
      - if:
          anyOf:
            - required:
              - revenueMin
            - required:
              - revenueMax
            - required:
              - revenueSd
        then:
          required:
            - revenueStatsDefinition
