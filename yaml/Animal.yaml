class:
  name: Animal
  type: Blank Node
  examples:
    - animal.jsonld
  doc: >
    A type of animal present during the [Cycle](/schema/Cycle).
    Adding multiple blank nodes of this type specifies the population ("herd composition")
    or describes a polyculture system.
    Use <code>[properties](/schema/Animal#properties)</code> to add information describing each
    animal type, such as [Liveweight per head](/term/liveweightPerHead),
    [liveweight gain](/term/liveweightGain), [age](/term/age),
    [mortality rate](/term/mortalityRate), or [pregnancy rate total](/term/pregnancyRateTotal).
    Use <code>[inputs](/schema/Animal#inputs)</code> to specify the Inputs for each animal type such
    as the feed use.
    Input quantities should be the total for all animals represented by this blank node and not the
    input quantity per head.
    Use <code>[practices](/schema/Animal#practices)</code> to specify Practices describing each
    each animal type such as the [animal breed](/glossary?page=1&termType=animalBreed).

  properties:
    - name: animalId
      type: string
      doc: An identifier for each Animal which must be unique within the Cycle.
      required: true
      exportDefaultCSV: true

    - name: term
      type: Ref[Term]
      doc: A reference to the [Term](/schema/Term) describing the Animal.
      required: true
      searchable: true
      exportDefaultCSV: true

    - name: description
      type: string
      doc: A description of the Animal type.
      exportDefaultCSV: true

    - name: referencePeriod
      type: string
      enum:
        - average
        - start of Cycle
        - end of Cycle
      doc: >
        <ul class="is-pl-3 is-list-style-disc">
          <li>
            The data are a time-weighted <code>average</code> over the Cycle. The recommended
            value.
          </li>
          <li>
            The data describe the Animal at the <code>start of Cycle</code>.
          </li>
          <li>
            The data describe the Animal at the <code>end of Cycle</code>.
          </li>
        </ul>
      required: true
      exportDefaultCSV: true

    - name: value
      type: number
      doc: >
        The number of Animals per <code>[functionalUnit](/schema/Cycle#functionalUnit)</code>.
        If using an average reference period, the number should be a time-weighted average
        which takes into account transitions between different categories that occur during
        the Cycle (e.g., female calves becoming heifers) and mortalities.
      recommended: true
      exportDefaultCSV: true

    - name: sd
      type: number
      minimum: 0
      doc: The standard deviation of <code>[value](/schema/Animal#value)</code>.
      exportDefaultCSV: true

    - name: min
      type: number
      doc: The minimum of <code>[value](/schema/Animal#value)</code>.
      exportDefaultCSV: true

    - name: max
      type: number
      doc: The maximum of <code>[value](/schema/Animal#value)</code>.
      exportDefaultCSV: true

    - name: statsDefinition
      type: string
      enum:
        - sites
        - cycles
        - replications
        - other observations
        - time
        - spatial
        - regions
        - simulated
        - modelled
      doc: >
        What the descriptive statistics (<code>[sd](/schema/Animal#sd)</code>, <code>[min](/schema/Animal#min)</code>,
        <code>[max](/schema/Animal#max)</code>, and <code>[value](/schema/Animal#value)</code>) are calculated across, or
        whether they are simulated or the output of a model. <code>spatial</code> refers to
        descriptive statistics calculated across spatial units (e.g., pixels) within a region
        or country. <code>time</code> refers to descriptive statistics calculated across units
        of time (e.g., hours).
      exportDefaultCSV: true

    - name: observations
      type: number
      doc: The number of observations the descriptive statistics are calculated over.
      searchable: true
      minimum: 1
      exportDefaultCSV: true

    - name: price
      type: number
      minimum: 0
      doc: >
        The price of the Animal. The price should be expressed per animal.
        The [currency](/schema/Animal#currency) must be specified.
      exportDefaultCSV: true

    - name: currency
      type: string
      pattern: ^[A-Z]{3}$
      doc: >
        The three letter currency code in [ISO 4217](https://en.wikipedia.org/wiki/ISO_4217)
        format.
      exportDefaultCSV: true

    - name: methodClassification
      type: string
      enum:
        - physical measurement
        - verified survey data
        - non-verified survey data
        - modelled
        - estimated with assumptions
        - consistent external sources
        - inconsistent external sources
        - expert opinion
        - unsourced assumption
      doc: >
        A classification of the method used to acquire or estimate the
        <code>[term](/schema/Animal#term)</code> and <code>[value](/schema/Animal#value)</code>. Overrides the
        <code>[defaultMethodClassification](/schema/Cycle#defaultMethodClassification)</code>
        specified in the [Cycle](/schema/Cycle). <code>methodClassification</code> should be specified
        separately for <code>[properties](/schema/Animal#properties)</code>
        (see <code>[Property](/schema/Property#methodClassification)</code>),
        <code>[inputs](/schema/Animal#inputs)</code>
        (see <code>[Input](/schema/Input#methodClassification)</code>) and
        <code>[practices](/schema/Animal#practices)</code> (see
        <code>[Practice](/schema/Practice#methodClassification)</code>).
        <ul class="is-pl-3 is-list-style-disc">
          <li>
            <code>physical measurement</code> means the amount is quantified using weighing,
            volume measurement, metering, chemical methods, or other physical approaches.
          </li>
          <li>
            <code>verified survey data</code> means the data are initially collected through
            surveys; all or a subset of the data are verified using physical methods; and
            erroneous survey data are discarded or corrected.
          </li>
          <li>
            <code>non-verified survey data</code> means the data are collected through
            surveys that have not been subjected to verification.
          </li>
          <li>
            <code>modelled</code> means a previously calibrated model is used to estimate
            this data point from other data points describing this Cycle.
          </li>
          <li>
            <code>estimated with assumptions</code> means a documented assumption is used
            to estimate this data point from other data points describing this Cycle.
          </li>
          <li>
            <code>consistent external sources</code> means the data are taken from external
            datasets referring to different producers/enterprises:
            <ul class="is-pl-4 is-list-style-disc">
              <li>
                Using the same technology (defined as the same
                [System](/glossary?termType=system) or the same key [Practices](/schema/Practice)
                as those specified in the Cycle);
              </li>
              <li>
                At the same date (defined as occurring within the
                [startDate](/schema/Cycle#startDate) and [endDate](/schema/Cycle#endDate) of the Cycle);
                and
              </li>
              <li>
                In the same [region](/schema/Site#region) or [country](/schema/Site#country).
              </li>
            </ul>
            Modelling or assumptions may have also been used to transform these data.
          </li>
          <li>
            <code>inconsistent external sources</code> means the data are taken from external
            datasets referring to different producers/enterprises:
            <ul class="is-pl-4 is-list-style-disc">
              <li>
                Using a different technology (defined as a different
                [System](/glossary?termType=system) or using different key
                [Practices](/schema/Practice) to those specified in the Cycle);
              </li>
              <li>
                At a different date (defined as occurring within the
                [startDate](/schema/Cycle#startDate) and [endDate](/schema/Cycle#endDate) of the Cycle);
                or
              </li>
              <li>
                In a different [region](/schema/Site#region) or [country](/schema/Site#country).
              </li>
            </ul>
            Modelling or assumptions may have also been used to transform these data.
          </li>
          <li>
            <code>expert opinion</code> means the data have been estimated by experts in
            the field.
          </li>
          <li>
            <code>unsourced assumption</code> means the data do not have a clear source
            and/or are based on assumptions only.
          </li>
        </ul>
      searchable: true
      exportDefaultCSV: true

    - name: methodClassificationDescription
      type: string
      doc: >
        A justification of the <code>[methodClassification](/schema/Animal#methodClassification)</code>
        used. If the data were <code>estimated with assumptions</code> this field should
        also describe the assumptions. This is a required field if
        <code>[methodClassification](/schema/Animal#methodClassification)</code> is specified.
      exportDefaultCSV: true

    - name: source
      type: Ref[Source]
      doc: >
        A reference to the [Source](/schema/Source) of these data, if different from the
        [defaultSource](/schema/Cycle#defaultSource) of the [Cycle](/schema/Cycle).
      exportDefaultCSV: true

    - name: properties
      type: List[Property]
      doc: >
        A list of [Properties](/schema/Property) of the Animal type, which would override any
        default properties specified in the [term](/schema/Animal#term).
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - key.@id
        - date
        - startDate
        - endDate

    - name: inputs
      type: List[Input]
      doc: >
        The Inputs (e.g., feed or veterinary drugs).
        Values for each Input should be a sum over all animals represented by this blank
        node and not a value per head.
      searchable: true
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - dates
        - startDate
        - endDate
        - isAnimalFeed
        - producedInCycle
        - transport.term.@id
        - operation.@id
        - country.@id
        - region.@id
        - impactAssessment.id

    - name: practices
      type: List[Practice]
      doc: >
        The Practices used to describe the
        [system](/glossary?page=1&termType=system) each Animal
        type is in or to describe management practices specific to each animal type
        (e.g., the [Milk yield per cow (FPCM)](/term/milkYieldPerCowFpcm)).
      searchable: true
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - key.@id
        - dates
        - startDate
        - endDate
        - areaPercent
        - ownershipStatus

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true

    - name: aggregated
      type: array[string]
      doc: A list of fields that have been 'aggregated' using data from multiple [Cycles](/schema/Cycle).
      internal: true

    - name: aggregatedVersion
      type: array[string]
      doc: A list of versions of the aggregation engine corresponding to each aggregated field.
      internal: true

  validation:
    allOf:
      - if:
          required:
            - term
        then:
          properties:
            term:
              properties:
                termType:
                  enum:
                    - liveAnimal
                    - liveAquaticSpecies
      - if:
          anyOf:
            - required:
              - min
            - required:
              - max
            - required:
              - sd
        then:
          required:
            - statsDefinition
      - if:
          required:
            - statsDefinition
        then:
          anyOf:
            - required:
              - value
            - required:
              - min
            - required:
              - max
            - required:
              - sd
      - if:
          anyOf:
            - required:
              - price
        then:
          required:
            - currency
      - if:
          required:
            - methodClassification
        then:
          required:
            - methodClassificationDescription
