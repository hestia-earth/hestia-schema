class:
  name: ImpactAssessment
  type: Node
  examples:
    - impactAssessment.jsonld
  doc: >
    The emissions, resource uses, and environmental impacts created during the production
    of one unit of a [Product](/schema/ImpactAssessment#product) from a [Cycle](/schema/Cycle). The functional unit is defined
    by the units of the Product. If there are more than one Product in the Cycle, allocation is
    used to apportion these impacts across the Products.

  properties:
    - name: name
      type: string
      doc: The name of the Impact Assessment.
      searchable: true

    - name: version
      type: string
      doc: The version of the Impact Assessment.

    - name: versionDetails
      type: string
      doc: A text description of the version of the Impact Assessment.

    - name: cycle
      type: Ref[Cycle]
      doc: A reference to the node describing the production [Cycle](/schema/Cycle).
      searchable: true
      exportDefaultCSV: true
      recommended: true

    - name: product
      type: Embed[Product]
      doc: >
        The [Product](/schema/Product) produced during the production [Cycle](/schema/Cycle), which is the target of this Impact Assessment.
      required: true
      searchable: true
      exportDefaultCSV: true

    - name: functionalUnitQuantity
      type: number
      const: 1
      doc: >
        The quantity of the <code>[product](/schema/ImpactAssessment#product)</code> that this Impact Assessment is
        expressed per. The units are determined by the [Term](/schema/Term) of the <code>[product](/schema/ImpactAssessment#product)</code>
        (e.g., 1kg for [Wheat, grain](/term/wheatGrain), or 1 head for a [Pig](/term/pig)).
      required: true
      exportDefaultCSV: true

    - name: allocationMethod
      type: string
      enum:
        - economic
        - mass
        - energy
        - biophysical
        - none
        - none required
        - system expansion
      doc: The method used to allocate environmental impacts between [Products](/schema/Product).
      required: true
      exportDefaultCSV: true

    - name: endDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      datePattern: true
      doc: >
        The end date or year of production in
        [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD, YYYY-MM, or YYYY).
      required: true
      searchable: true
      exportDefaultCSV: true

    - name: startDate
      type: string
      pattern: ^[0-9]{4}(-[0-9]{2})?(-[0-9]{2})?$
      datePattern: true
      doc: >
        The start date of production in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601)
        format (YYYY-MM-DD, YYYY-MM, or YYYY).
      exportDefaultCSV: true

    - name: site
      type: Ref[Site]
      doc: A reference to the node describing the [Site](/schema/Site) where production occurred.
      searchable: true
      exportDefaultCSV: true
      recommended: true

    - name: country
      type: Ref[Term]
      doc: >
        The country from the [Glossary](/glossary?termType=region),
        following [GADM](https://gadm.org/) naming conventions.
      required: true
      searchable: true
      exportDefaultCSV: true

    - name: region
      type: Ref[Term]
      doc: >
        The lowest level [GADM](https://gadm.org/) region available following the naming
        convention used in the [Glossary](/glossary?termType=region).
      searchable: true
      exportDefaultCSV: true

    - name: organisation
      type: Ref[Organisation]
      doc: >
        A reference to the node describing the [Organisation](/schema/Organisation) that produced the [Product](/schema/Product).
      searchable: true

    - name: source
      type: Ref[Source]
      doc: >
        The [Source](/schema/Source) for the data in the Impact Assessment.
        Not required (but recommended) for public uploads (i.e.,
        where <code>[dataPrivate](/schema/ImpactAssessment#dataPrivate)</code> is <code>false</code>).
      searchable: true
      exportDefaultCSV: true
      recommended: true

    - name: emissionsResourceUse
      type: List[Indicator]
      doc: >
        A list of emissions and resource uses.
      searchable: true
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - inputs.@id
        - operation.@id
        - methodModel.@id
        - transformation.@id
        - landCover.@id
        - previousLandCover.@id

    - name: impacts
      type: List[Indicator]
      doc: >
        The mid-point environmental impact [Indicators](/schema/Indicator). These are
        calculated from <code>[emissions](/schema/ImpactAssessment#emissions)</code> and
        <code>[resourceUse](/schema/ImpactAssessment#resourceUse)</code> by applying characterisation factors
        to generate a [characterised impact indicator](/glossary?termType=characterisedIndicator).
      searchable: true
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - inputs.@id
        - methodModel.@id

    - name: endpoints
      type: List[Indicator]
      doc: >
        The end-point environmental impact [Indicators](/schema/Indicator). These are
        calculated from the mid-point <code>[impacts](/schema/ImpactAssessment#impacts)</code>
        by applying characterisation factors to generate an
        [end-point impact indicator](/glossary?termType=endpointIndicator).
      searchable: true
      exportDefaultCSV: true
      uniqueArrayItem:
        - term.@id
        - inputs.@id
        - methodModel.@id

    - name: dataPrivate
      type: boolean
      doc: >
        If these data are private. Private means that HESTIA administrators can access these data and
        you can grant access to other platform users, but these data will not be made available to
        any other users of the platform or distributed to third parties.
      required: true
      default: false

    - name: organic
      type: boolean
      doc: If the [Cycle](/schema/ImpactAssessment#cycle) has an organic label. Used by the aggregation engine only.
      internal: true
      default: false

    - name: irrigated
      type: boolean
      doc: If the [Cycle](/schema/ImpactAssessment#cycle) was irrigated. Used by the aggregation engine only.
      internal: true
      default: false

    - name: autoGenerated
      type: boolean
      doc: If this node was autogenerated during upload.
      internal: true
      default: false

    - name: originalId
      type: string
      doc: >
        The identifier for these data in the source database (e.g., if the data were converted
        from openLCA or ecoinvent, the id field from that database).
      internal: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true
      searchable: true

    - name: added
      type: array[string]
      doc: A list of fields that have been added to the original dataset.
      internal: true

    - name: addedVersion
      type: array[string]
      doc: A list of versions of the model used to add these fields.
      internal: true

    - name: updated
      type: array[string]
      doc: A list of fields that have been updated on the original dataset.
      internal: true

    - name: updatedVersion
      type: array[string]
      doc: A list of versions of the model used to update these fields.
      internal: true

    - name: aggregated
      type: boolean
      doc: If this Impact Assessment has been 'aggregated' using data from multiple Impact Assessments.
      internal: true
      searchable: true

    - name: aggregatedDataValidated
      type: boolean
      doc: If this aggregated Impact Assessment has been validated by the HESTIA team.
      internal: true
      searchable: true

    - name: aggregatedVersion
      type: string
      doc: A version of the aggregation engine corresponding to this Impact Assessment.
      internal: true

    - name: aggregatedQualityScore
      type: integer
      doc: >
        A data quality score for aggregated data, set equal to
        [the quality score of the linked Cycle](/schema/Cycle#aggregatedQualityScore).
      minimum: 0
      internal: true
      searchable: true

    - name: aggregatedQualityScoreMax
      type: integer
      doc: The maximum value for [the aggregated quality score](/schema/ImpactAssessment#aggregatedQualityScore), set equal to
        [the max quality score of the linked Cycle](/schema/Cycle#aggregatedQualityScoreMax).
      minimum: 3
      internal: true
      searchable: true

    - name: aggregatedImpactAssessments
      type: List[Ref[ImpactAssessment]]
      doc: Impact Assessments used to aggregated this Impact Assessment.
      internal: true

    - name: aggregatedSources
      type: List[Ref[Source]]
      doc: Sources used to aggregated this Impact Assessment.
      internal: true

    - name: createdAt
      type: date
      format: date
      doc: >
        Date created on HESTIA in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      internal: true
      searchable: true

    - name: updatedAt
      type: date
      format: date
      doc: >
        Last update date on HESTIA in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      internal: true
      searchable: true

  validation:
    allOf:
      - if:
          required:
            - dataPrivate
          properties:
            dataPrivate:
              const: false
        then:
          required:
            - source
      - if:
          required:
            - country
        then:
          properties:
            country:
              properties:
                termType:
                  enum:
                    - region
      - if:
          required:
            - region
        then:
          properties:
            region:
              properties:
                termType:
                  enum:
                    - region
      - if:
          required:
            - emissionsResourceUse
        then:
          properties:
            emissionsResourceUse:
              items:
                properties:
                  term:
                    properties:
                      termType:
                        enum:
                          - emission
                          - resourceUse
      - if:
          required:
            - impacts
        then:
          properties:
            impacts:
              items:
                properties:
                  term:
                    properties:
                      termType:
                        enum:
                          - characterisedIndicator
      - if:
          required:
            - endpoints
        then:
          properties:
            endpoints:
              items:
                properties:
                  term:
                    properties:
                      termType:
                        enum:
                          - endpointIndicator
      - if:
          required:
            - impacts
        then:
          properties:
            impacts:
              items:
                required:
                  - methodModel
      - if:
          required:
            - endpoints
        then:
          properties:
            impacts:
              items:
                required:
                  - methodModel
