class:
  name: Actor
  type: Node
  examples:
    - actor.jsonld
  doc: A person or institution.

  properties:
    - name: name
      type: string
      doc: >
        An automatically generated field made up of: <code>firstName</code> initial,
        <code>lastName</code>, <code>primaryInstitution</code>. If the Actor is an
        institution, the <code>lastName</code> only.
      extend: true
      internal: true
      searchable: true

    - name: firstName
      type: string
      doc: The Actor's first name.

    - name: lastName
      type: string
      doc: The Actor's last name or the name of the institution.
      required: true

    - name: orcid
      type: string
      doc: The Actor's [ORCiD](https://orcid.org/) identifier.
      unique: true
      searchable: true

    - name: scopusID
      type: string
      doc: The Actor's [Scopus](https://www.scopus.com/) identifier.
      unique: true
      searchable: true

    - name: primaryInstitution
      type: string
      doc: The Actor's primary institution.

    - name: city
      type: string
      doc: A city or town.

    - name: country
      type: Ref[Term]
      doc: >
        The country from the [Glossary](/glossary?termType=region),
        following [GADM](https://gadm.org/) naming conventions.

    - name: email
      type: string
      doc: The email address of the Actor.
      format: email
      extend: true
      searchable: true
      unique: true

    - name: website
      type: iri
      doc: A link to a website describing the Actor.

    - name: dataPrivate
      type: boolean
      doc: >
        If these data are private. Private means that HESTIA administrators can access these data and
        you can grant access to other platform users, but these data will not be made available to
        any other users of the platform or distributed to third parties.
      required: true
      default: false

    - name: originalId
      type: string
      doc: >
        The identifier for these data in the source database (e.g., if the data were converted
        from openLCA or ecoinvent, the id field from that database).
      internal: true

    - name: schemaVersion
      type: string
      doc: The version of the schema when these data were created.
      internal: true
      searchable: true

    - name: createdAt
      type: date
      format: date
      doc: >
        Date created on HESTIA in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      internal: true
      searchable: true

    - name: updatedAt
      type: date
      format: date
      doc: >
        Last update date on HESTIA in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format (YYYY-MM-DD).
      internal: true
      searchable: true

  validation:
    allOf:
      - if:
          required:
            - country
        then:
          properties:
            country:
              properties:
                termType:
                  enum:
                    - region
