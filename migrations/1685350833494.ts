const findNodesQuery = {
  bool: {
    must: [{ match: { '@type': 'ImpactAssessment' } }]
  }
};

const migrateNode = ({ product, ...node }: any) => {
  return {
    ...node,
    product: {
      '@type': 'Product',
      term: product
    }
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
