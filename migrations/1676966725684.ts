const findNodesQuery = {
  bool: {
    should: [{ match: { '@type': 'Cycle' } }, { match: { '@type': 'Site' } }],
    minimum_should_match: 1
  }
};

const field = 'methodClassification';
const oldValue = 'unsourced or assumption';
const newValue = 'unsourced assumption';

const migrateBlankNodes = (node: any, listKey: string) => {
  if (listKey in node) {
    const values = node[listKey];
    return {
      [listKey]: values.map(value => {
        if (field in value && value[field] === oldValue) {
          value[field] = newValue;
        }
        return {
          ...value,
          ...migrateBlankNodes(value, 'transport')
        };
      })
    };
  }
  return {};
};

const migrateNode = (node: any) => {
  return {
    ...node,
    ...migrateBlankNodes(node, 'emissions'),
    ...migrateBlankNodes(node, 'infrastructure'),
    ...migrateBlankNodes(node, 'inputs'),
    ...migrateBlankNodes(node, 'practices'),
    ...migrateBlankNodes(node, 'products')
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
