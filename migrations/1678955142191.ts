const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Cycle' } },
      { match: { 'defaultMethodClassification.keyword': 'unsourced or assumption' } }
    ]
  }
};

const migrateNode = (node: any) => {
  return {
    ...node,
    defaultMethodClassification: 'unsourced assumption'
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
