const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Cycle' } },
      {
        exists: {
          field: 'completeness'
        }
      }
    ]
  }
};

const migrateNode = ({
  completeness: { other, pesticidesAntibiotics, liveAnimals, operations, soilAmendments, products, ...completeness },
  ...node
}: any) => {
  return {
    ...node,
    completeness: {
      ...completeness,
      seed: other,
      pesticideVeterinaryDrug: pesticidesAntibiotics,
      liveAnimalInput: liveAnimals,
      operation: operations,
      soilAmendment: soilAmendments,
      product: products,
      otherChemical: false,
      grazedForage: false,
      ingredient: false
    }
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
