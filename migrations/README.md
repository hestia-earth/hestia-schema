# Schema Migrations

The schema migrations contain changes made in the schema that can be automatically updated on every indexed Node.

## Process

1. Generate a new migration: `node scripts/generate-migration.js`.
1. Edit the newly created migration: the `findNodesQuery` method must return a query that will be used to find the nodes to migrate, while the `migrateNode` method takes one of the found Node as param and must return it.
1. Commit on a branch and create a Merge Request
