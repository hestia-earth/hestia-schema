const findNodesQuery = {
  bool: {
    must: [{ match: { '@type': 'Site' } }]
  }
};

const field = 'methodClassification';

const migrateBlankNodes = (node: any, listKey: string, oldValue: string, newValue: string) => {
  if (listKey in node) {
    const values = node[listKey];
    return {
      [listKey]: values.map(value => {
        if (field in value && value[field] === oldValue) {
          value[field] = newValue;
        }
        return value;
      })
    };
  }
  return {};
};

const migrateNode = (node: any) => {
  return {
    ...node,
    ...migrateBlankNodes(node, 'measurements', 'on-site measurement', 'on-site physical measurement'),
    ...migrateBlankNodes(node, 'measurements', 'measurement on nearby site', 'physical measurement on nearby site')
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
