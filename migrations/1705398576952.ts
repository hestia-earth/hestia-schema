const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Cycle' } },
      {
        exists: {
          field: 'completeness.animalHerd'
        }
      }
    ]
  }
};

const migrateNode = ({ completeness: { animalHerd, ...completeness }, ...node }: any) => {
  return {
    ...node,
    completeness: {
      ...completeness,
      animalPopulation: animalHerd
    }
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
