const findNodesQuery = {
  bool: {
    must: [{ match: { '@type': 'Cycle' } }]
  }
};

const migrateNode = (cycle: any) => {
  const operations = [
    (cycle.emissions || []).some(blankNode => !!blankNode?.operation),
    (cycle.inputs || []).some(blankNode => !!blankNode?.operation),
    (cycle.practices || []).some(blankNode => blankNode?.term?.termType === 'operation')
  ].some(Boolean);
  return {
    ...cycle,
    completeness: {
      ...cycle.completeness,
      operations
    }
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
