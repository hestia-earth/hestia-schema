const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Cycle' } },
      {
        exists: {
          field: 'completeness.grazedForage'
        }
      }
    ]
  }
};

const migrateNode = ({ completeness: { grazedForage, ...completeness }, ...node }: any) => {
  return {
    ...node,
    completeness: {
      ...completeness,
      freshForage: grazedForage
    }
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
