const findNodesQuery = {
  bool: {
    must: [
      { match: { '@type': 'Site' } },
      {
        nested: {
          path: 'measurements',
          query: {
            match: { 'measurements.methodClassification.keyword': 'modelled using other physical measurements' }
          }
        }
      }
    ]
  }
};

const migrateMeasurement = (measurement: any) => {
  if (measurement.methodClassification === 'modelled using other physical measurements') {
    measurement.methodClassification = 'modelled using other measurements';
  }
  return measurement;
};

const migrateNode = ({ measurements, ...node }: any) => {
  return {
    ...node,
    measurements: (measurements || []).map(migrateMeasurement)
  };
};

module.exports = {
  findNodesQuery,
  migrateNode
};
