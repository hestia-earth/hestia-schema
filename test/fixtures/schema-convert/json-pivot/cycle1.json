{
  "@context": "https://DOMAIN/schema/Cycle.jsonld",
  "@id": "vc664x8",
  "@type": "Cycle",
  "name": "Wheat, grain - Forbach, France - 1989",
  "site": {
    "@id": "bbscc9",
    "@type": "Site",
    "name": "Field 1",
    "siteType": "cropland",
    "country": {
      "@id": "GADM-FRA",
      "@type": "Term",
      "name": "France",
      "termType": "region"
    }
  },
  "defaultMethodClassification": "verified survey data",
  "defaultMethodClassificationDescription": "Data from a postal survey of 200 farms where researchers followed up with on-site visits to validate the data.",
  "defaultSource": {
    "@id": "s66765d",
    "@type": "Source",
    "name": "Gigou (1990)"
  },
  "endDate": "1989-12-31",
  "startDate": "1989-06-01",
  "startDateDefinition": "harvest of previous crop",
  "cycleDuration": 213,
  "siteDuration": 213,
  "siteArea": 1,
  "description": "50N treatment",
  "commercialPracticeTreatment": false,
  "functionalUnit": "1 ha",
  "completeness": {
    "@type": "Completeness",
    "animalPopulation": true,
    "freshForage": true,
    "ingredient": true,
    "otherChemical": true,
    "operation": false,
    "electricityFuel": false,
    "material": true,
    "transport": true,
    "fertiliser": true,
    "soilAmendment": true,
    "pesticideVeterinaryDrug": true,
    "water": true,
    "animalFeed": true,
    "liveAnimalInput": true,
    "seed": false,
    "product": true,
    "cropResidue": false,
    "excreta": true,
    "waste": false
  },
  "inputs": [
    {
      "@type": "Input",
      "term": {
        "@id": "CAS-34494-04-7",
        "@type": "Term",
        "name": "Glyphosate",
        "units": "g active ingredient",
        "termType": "pesticideAI"
      },
      "value": [400],
      "sd": [180],
      "statsDefinition": "replications",
      "observations": [10]
    },
    {
      "@type": "Input",
      "term": {
        "@id": "ureaKgN",
        "@type": "Term",
        "name": "Urea (kg N)",
        "units": "kg N",
        "termType": "inorganicFertiliser"
      },
      "value": [40],
      "transport": [
        {
          "@type": "Transport",
          "term": {
            "@id": "freightLorry32MetricTon",
            "@type": "Term",
            "name": "Freight, lorry, >32 metric ton",
            "units": "tkm",
            "termType": "transport"
          },
          "value": 8,
          "returnLegIncluded": false,
          "methodClassification": "estimated with assumptions",
          "methodClassificationDescription": "Distance estimated using approximate road distance to likely supplier, assuming 100% load factor.",
          "emissions": [
            {
              "@type": "Emission",
              "term": {
                "@id": "co2ToAirFuelCombustion",
                "@type": "Term",
                "name": "CO2, to air, fuel combustion",
                "units": "kg CO2",
                "termType": "emission"
              },
              "value": [0.7],
              "methodModel": {
                "@id": "emepEea2019",
                "@type": "Term",
                "name": "EMEA-EEA (2019)",
                "termType": "model"
              },
              "methodTier": "tier 1"
            }
          ]
        }
      ]
    },
    {
      "@type": "Input",
      "term": {
        "@id": "electricityGridMarketMix",
        "@type": "Term",
        "name": "Electricity, grid, market mix",
        "units": "kWh",
        "termType": "electricity"
      },
      "value": [600]
    },
    {
      "@type": "Input",
      "term": {
        "@id": "fixedNitrogenFromPreviousCropKgN",
        "@type": "Term",
        "name": "Fixed nitrogen, from previous crop (kg N)",
        "units": "kg N",
        "termType": "organicFertiliser"
      },
      "value": [40],
      "methodClassification": "modelled",
      "methodClassificationDescription": "Modelled using a simple model which links nitrogen fixation to nitrogen fertiliser input (Jensen, 1987, Plant and Soil)."
    }
  ],
  "emissions": [
    {
      "@type": "Emission",
      "term": {
        "@id": "no3ToGroundwaterSoilFlux",
        "@type": "Term",
        "name": "NO3, to groundwater, soil flux",
        "units": "kg NO3",
        "termType": "emission"
      },
      "value": [60.75, 70.5],
      "dates": ["1989-06-16", "1989-08-01"],
      "methodModel": {
        "@id": "percolationLysimeter",
        "@type": "Term",
        "name": "Percolation lysimeter",
        "termType": "methodEmissionResourceUse"
      },
      "methodTier": "measured",
      "source": {
        "@id": "source-8v5",
        "@type": "Source",
        "name": "Murwira (1993)"
      }
    },
    {
      "@type": "Emission",
      "term": {
        "@id": "n2OToAirInorganicFertiliserDirect",
        "@type": "Term",
        "name": "N2O, to air, inorganic fertiliser, direct",
        "units": "kg N2O",
        "termType": "emission"
      },
      "value": [0.628],
      "methodModel": {
        "@id": "ipcc2006",
        "@type": "Term",
        "name": "IPCC (2006)",
        "termType": "model"
      },
      "methodTier": "tier 1"
    },
    {
      "@type": "Emission",
      "term": {
        "@id": "so2ToAirInputsProduction",
        "@type": "Term",
        "name": "SO2, to air, inputs production",
        "units": "kg SO2",
        "termType": "emission"
      },
      "value": [2.11],
      "methodModel": {
        "@id": "ecoinventV3",
        "@type": "Term",
        "name": "ecoinvent v3",
        "termType": "model"
      },
      "methodTier": "background",
      "inputs": [
        {
          "@id": "electricityGridMarketMix",
          "@type": "Term",
          "name": "Electricity, grid, market mix",
          "termType": "electricity"
        }
      ]
    }
  ],
  "products": [
    {
      "@type": "Product",
      "term": {
        "@id": "wheatGrain",
        "@type": "Term",
        "name": "Wheat, grain",
        "units": "kg",
        "termType": "crop"
      },
      "value": [7282],
      "primary": true,
      "economicValueShare": 94.8,
      "properties": [
        {
          "@type": "Property",
          "term": {
            "@id": "dryMatter",
            "@type": "Term",
            "name": "Dry matter",
            "units": "%",
            "termType": "property"
          },
          "value": 80
        }
      ]
    },
    {
      "@type": "Product",
      "term": {
        "@id": "wheatStraw",
        "@type": "Term",
        "name": "Wheat, straw",
        "units": "kg",
        "termType": "crop"
      },
      "value": [40],
      "economicValueShare": 5.2
    }
  ],
  "practices": [
    {
      "@type": "Practice",
      "term": {
        "@id": "croppingDuration",
        "@type": "Term",
        "name": "Cropping duration",
        "units": "days",
        "termType": "landUseManagement"
      },
      "value": [163]
    },
    {
      "@type": "Practice",
      "term": {
        "@id": "shortFallowDuration",
        "@type": "Term",
        "name": "Short fallow duration",
        "units": "days",
        "termType": "landUseManagement"
      },
      "value": [50]
    },
    {
      "@type": "Practice",
      "term": {
        "@id": "seedTreated",
        "@type": "Term",
        "name": "Seed treated",
        "termType": "cropEstablishment"
      }
    }
  ],
  "dataPrivate": false
}
