import { readFileSync } from 'fs';
import { join } from 'path';

export const FIXTURES_FOLDER = join(__dirname, 'fixtures');
const ENCODING = 'utf8';

export const readFileAsString = (file: string, folder = FIXTURES_FOLDER) =>
  readFileSync(join(folder, file), ENCODING).trim();

export const readFileAsJson = (file: string, folder = FIXTURES_FOLDER) => JSON.parse(readFileAsString(file, folder));
