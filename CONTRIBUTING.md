# Contributing to the HESTIA Schema Development

If you are an external user and would like to see changes to the HESTIA schema, please [create an Issue](#submit-issue).

If you have been granted access to the HESTIA schema GitLab, please follow the below instructions to make changes to the schema.

 - [Submitting and Issue](#submit-issue)
 - [Updating the Schema](#update-schema)
 - [Commit Message Guidelines](#commit)

# <a name="submit-issue"></a> Submitting an Issue

Before you submit an issue, please search the issue tracker, maybe an issue for your problem already exists and the discussion might inform you of workarounds readily available.

## <a name="issue"></a> Found a Bug?
If you find a bug, you can help us by [submitting an issue](#submit-issue) and using the bug template to our [GitLab Repository](https://gitlab.com/hestia-earth/hestia-schema).

Before fixing a bug we need to reproduce and confirm it. In order to reproduce bugs, we will systematically ask you to provide a minimal reproduction. Having a minimal reproducible scenario gives us a wealth of important information without going back & forth to you with additional questions.

A minimal reproduction allows us to quickly confirm a bug (or point out a coding problem) as well as confirm that we are fixing the right problem.

We will be insisting on a minimal reproduction scenario in order to save maintainers time and ultimately be able to fix more bugs. Interestingly, from our experience, users often find coding problems themselves while preparing a minimal reproduction. We understand that sometimes it might be hard to extract essential bits of code from a larger codebase but we really need to isolate the problem before we can fix it.

Unfortunately, we are not able to investigate / fix bugs without a minimal reproduction, so if we don't hear back from you, we are going to close an issue that doesn't have enough info to be reproduced.

## <a name="feature"></a> Missing a Feature?
You can *request* a new feature by [submitting an issue](#submit-issue) and using the feature template to our GitLab
Repository.

# <a name="update-schema"></a> Updating the Schema

If you have access to the schema repository on GitLab, you can update the schema directly using a merge request.

After making changes to the schema, you will need to run a few commands to make sure everything is valid and commit necessary changes.

To set up, first do:

1. Install latest LTS [Node.js](https://nodejs.org/en/download/) version
2. Run `npm install`

Now, when making changes, you will need to:

* Run `npm run build`: this will try to **generate** all the files from the `yaml` directory.
* Run `npm run validate:jsonld`: this will try to **validate** all the jsonld files in the `examples` folder.

Note: if any of the commands above fail, try making changes. If not, ask for a reviewer to check it for you.

## <a name="submit-mr"></a> Submitting a Merge Request (MR)
Before you submit your Merge Request (MR) consider the following guidelines:

1. Search [GitLab](https://gitlab.com/hestia-earth/hestia-schema/-/merge_requests) for an open or closed MR
  that relates to your submission. You don't want to duplicate effort.
1. Be sure that an issue describes the problem you're fixing, or documents the design for the feature you'd like to add.
  Discussing the design up front helps to ensure that we're ready to accept your work.
1. Fork the hestia-earth/schema repo.
1. Make your changes in a new git branch:

     ```shell
     git checkout -b my-fix-branch master
     ```

1. Create your patch, **including appropriate test cases**.
1. Commit your changes using a descriptive commit message that follows our
  [commit message conventions](#commit). Adherence to these conventions
  is necessary because release notes are automatically generated from these messages.

     ```shell
     git commit -a
     ```
    Note: the optional commit `-a` command line option will automatically "add" and "rm" edited files.

1. Push your branch to GitLab:

    ```shell
    git push origin my-fix-branch
    ```

1. In GitLab, send a pull request to `schema:master`.
* If we suggest changes then:
  * Make the required updates.
  * Re-run the HESTIA test suites to ensure tests are still passing.
  * Rebase your branch and force push to your GitLab repository (this will update your Merge Request):

    ```shell
    git rebase master -i
    git push -f
    ```

## After your pull request is merged

After your pull request is merged, you can safely delete your branch and pull the changes
from the main (upstream) repository:

* Delete the remote branch on GitLab either through the GitLab web UI or your local shell as follows:

    ```shell
    git push origin --delete my-fix-branch
    ```

* Check out the master branch:

    ```shell
    git checkout master -f
    ```

* Delete the local branch:

    ```shell
    git branch -D my-fix-branch
    ```

* Update your master with the latest upstream version:

    ```shell
    git pull --ff upstream master
    ```

# <a name="commit"></a> Commit Message Guidelines

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
we use the git commit messages to **generate the HESTIA change log**.

## Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer than 100 characters! This allows the message to be easier
to read on GitLab as well as in various git tools.

The footer should contain a [closing reference to an issue](https://help.github.com/articles/closing-issues-via-commit-messages/) if any.

Samples: (even more [samples](https://gitlab.com/hestia-earth/hestia-schema/-/commits/develop))

```
docs(changelog): update changelog to beta.5
```
```
fix(release): need to depend on latest rxjs and zone.js

The version in our package.json gets copied to the one we publish, and users need the latest of these.
```

## Revert
If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit. In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit being reverted.

## Type
Must be one of the following:

* **build**: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
* **ci**: Changes to our CI configuration files and scripts (example scopes: Circle, BrowserStack, SauceLabs)
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **perf**: A code change that improves performance
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **test**: Adding missing tests or correcting existing tests

## Scope
The scope should be the name of the npm package affected (as perceived by the person reading the changelog generated from commit messages).

## Subject
The subject contains a succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize the first letter
* no dot (.) at the end

## Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

## Footer
The footer should contain any information about **Breaking Changes** and is also the place to
reference GitLab issues that this commit **Closes**.

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines. The rest of the commit message is then used for this.
