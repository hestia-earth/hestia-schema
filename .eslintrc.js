module.exports = {
  env: {
    node: true
  },
  extends: [
    '@hestia-earth/eslint-config'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module'
  },
  "rules": {
    "@typescript-eslint/no-shadow": "off",
    "complexity": "off",
    "no-console": "off"
  }
};
