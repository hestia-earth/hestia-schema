#!/bin/sh

TMP_DIR="temp_dir"

rm -rf schema.zip
rm -rf "${TMP_DIR}"
mkdir -p "${TMP_DIR}"

cp -r src/@hestia-earth/json-schema/json-schema "${TMP_DIR}/json-schema"
cp -r context "${TMP_DIR}/context"
cp -r examples "${TMP_DIR}/examples"

cd "${TMP_DIR}"
zip -r ../schema.zip context examples json-schema

rm -rf "${TMP_DIR}"
