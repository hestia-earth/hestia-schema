import { readFileSync, readdirSync } from 'fs';
import { join } from 'path';
import { validateTerms } from '@hestia-earth/utils/dist/validate-terms';

const YAML_DIR = join(__dirname, '..', 'yaml');
const listYmlDir = () => readdirSync(YAML_DIR);
const readYmlFile = (file: string) => readFileSync(join(YAML_DIR, file), 'utf8');

const findTerms = (content: string) => content.match(/\/term\/([a-zA-Z\d]+)/g);

const cleanTerm = (content: string) => ({ '@id': content.replace('/term/', ''), name: null });

const run = async () => {
  const files = listYmlDir().map(readYmlFile);
  const terms = files.flatMap(findTerms).filter(Boolean).map(cleanTerm);
  const missing = await validateTerms(terms);
  if (missing.length) {
    throw new Error(`Terms not found: ${missing.join(', ')}`);
  }
};

run()
  .then(() => {
    console.log('Done validating terms');
    process.exit(0);
  })
  .catch(err => {
    console.log('Error validating terms.');
    console.error(err);
    process.exit(1);
  });
