const { readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const PACKAGE_PATH = resolve(join(ROOT, 'setup.py'));
const version = require(join(ROOT, 'package.json')).version;

let content = readFileSync(PACKAGE_PATH, 'UTF-8');
content = content.replace(/version=\'[\d\-a-z\.]+\'/, `version='${version}'`);
writeFileSync(PACKAGE_PATH, content, 'UTF-8');

const VERSIONS_PATH = resolve(join(ROOT, 'versions.txt'));
let versions = readFileSync(VERSIONS_PATH, 'UTF-8');
writeFileSync(VERSIONS_PATH, `${version}\n${versions}`.trim(), 'UTF-8');
