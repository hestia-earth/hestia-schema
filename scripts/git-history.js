const { exec } = require('child_process');
const { promises: { writeFile } } = require('fs');

const paths = process.argv.slice(2);

const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
const startYear = 2020;
const endYear = 2021;

const script = (start, end) => `
  git log \
    --since "${start}" \
    --until "${end}" \
    --pretty=tformat: --numstat ${paths.map(v => `"${v}"`).join(' ')} | gawk '{ add += $1 ; subs += $2 ; loc += $1 - $2 } END { printf "%s,%s",add,subs }'
`.trim();

const execCommand = (cmd) => {
  console.log('exec script', cmd);
  return new Promise((res, reject) =>
    exec(cmd, (err, stdout, stderr) => err || stderr ? reject(err || stderr) : res(stdout))
  );
};

const run = async () => {
  const contents = [];
  for (let year = startYear; year <= endYear; year++) {
    for (let index = 0; index < months.length - 1; index++) {
      const start = `${months[index]} 01 ${year}`;
      const end = `${months[index + 1]} 01 ${year}`;
      const date = `${months[index + 1]} ${year}`;
      const [added, removed] = (await execCommand(script(start, end))).split(',');
      contents.push([date, added, removed].join(','));
    }

    // add months between 2 dates
    const start = `${months[months.length - 1]} 01 ${year}`;
    const end = `${months[0]} 01 ${year + 1}`;
    const date = `${months[0]} ${year + 1}`;
    const [added, removed] = (await execCommand(script(start, end))).split(',');
    contents.push([date, added, removed].join(','));
  }
  await writeFile('./git-history.csv', `
date,added,removed
${contents.join('\n')}
  `.trim());
};

run().then(() => process.exit(0)).catch(err => {
  console.error(err);
  process.exit(1);
});
