const { readdirSync, readFileSync, writeFileSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '..'));
const yamlFolder = join(ROOT, 'yaml');
const yamlFiles = readdirSync(yamlFolder).map(f => join(yamlFolder, f));

console.log(yamlFiles);

const updateFile = filepath => {
  const content = readFileSync(filepath, 'utf-8');
  const nodeType = filepath.split('/').pop().split('.')[0];
  const newContent = content.replace(/(\[[\w\s.]+\])(\(#)(\w+\))/g, `$1(/schema/${nodeType}#$3`);
  writeFileSync(filepath, newContent, 'utf-8');
};

yamlFiles.map(updateFile);
