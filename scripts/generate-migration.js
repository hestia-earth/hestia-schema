const { writeFileSync, mkdirSync } = require('fs');
const { join, resolve } = require('path');
const { version } = require('../package.json');

const ROOT = resolve(join(__dirname, '..'));
const SRC_FOLDER = join(ROOT, 'migrations');
const TEST_FOLDER = join(ROOT, 'src', 'migrations');
const FIXTURES_FOLDER = join(ROOT, 'test', 'fixtures', 'migrations');

const id = new Date().getTime();
const srcTemplate = `
const findNodesQuery = {
  bool: {
    must: [{ match: { '@type': 'Cycle' } }]
  }
};

const migrateNode = (node: any) => {
  node.schemaVersion = '${+(version.split('.')[0]) + 1}.0.0';
  return node;
};

module.exports = {
  findNodesQuery,
  migrateNode
};

`.trimLeft();

const testTemplate = `
import { expect } from 'chai';
import 'mocha';

import { readFileAsJson } from '../../test/utils';
import { IMigration, readMigration } from './utils.spec';

const migrationNumber = '${id}';

describe('migrations > ${id}', () => {
  let migration: IMigration<any>;

  before(() => {
    migration = readMigration(migrationNumber);
  });

  describe('findNodesQuery', () => {
    it('should be defined', () => {
      expect(migration.findNodesQuery).not.to.equal(undefined);
    });
  });

  describe('migrateNode', () => {
    it('should migrate the data', () => {
      const original = readFileAsJson(\`migrations/\${migrationNumber}/original.json\`);
      const expected = readFileAsJson(\`migrations/\${migrationNumber}/expected.json\`);
      expect(migration.migrateNode(original)).to.deep.equal(expected);
    });
  });
});

`.trimLeft();

const fixturesTemplate ={
  "@type": "Cycle",
  "@id": "1"
};

const run = () => {
  console.log('generating source file...');
  writeFileSync(join(SRC_FOLDER, `${id}.ts`), srcTemplate, 'utf-8');
  console.log('generating test file...');
  writeFileSync(join(TEST_FOLDER, `${id}.spec.ts`), testTemplate, 'utf-8');

  console.log('generating fixture files...');
  const fixturesFolder = join(FIXTURES_FOLDER, `${id}`);
  mkdirSync(fixturesFolder, { recursive: true });
  writeFileSync(join(fixturesFolder, 'original.json'), JSON.stringify(fixturesTemplate, null, 2), 'utf-8');
  writeFileSync(join(fixturesFolder, 'expected.json'), JSON.stringify(fixturesTemplate, null, 2), 'utf-8');
};

run();
