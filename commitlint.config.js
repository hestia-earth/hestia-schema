const { readdirSync, statSync } = require('fs');
const { resolve, join } = require('path');

const root = resolve(__dirname);
const extension = '.yaml';
const typeScopes = readdirSync(join(root, 'yaml'))
  .filter(file => file.endsWith(extension))
  .map(file => file.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase())
  .map(file => file.replace(extension, ''));

const packageRoot = join(root, 'src', '@hestia-earth');
const packageScopes = readdirSync(packageRoot)
  .filter(folder => statSync(join(packageRoot, folder)).isDirectory());

const extraScopes = [
  '*',
  'assets',
  'examples',
  'templates',
  'layer',
  'migrations',
  'python',
  'fixtures',
  'scripts',
  'package',
  'readme',
  'contributing',
  'license',
  'gitlab',
  'serverless'
];

module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'scope-case': [0],
    'scope-enum': [2, 'always', [...typeScopes, ...packageScopes, ...extraScopes]],
    'footer-max-line-length': [2, 'always', 120]
  }
};
