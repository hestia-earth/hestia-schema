# HESTIA Schema Validation

Module to validate data using the HESTIA Schema and Ajv.

## Install

```sh
npm install @hestia-earth/schema @hestia-earth/schema-validation
```

## Example validating JSON

```javascript
const { SchemaType } = require('@hestia-earth/schema');
const { validator } = require('@hestia-earth/schema-validation/validate');

// Set to `true` to validate existing Nodes (@type/@id) or `false` for Nodes to upload (type/id)
const strictMode = false;
// Initialise the validation function, do this only once
const schemaValidation = validator(undefined, strictMode);

const node = {
  type: SchemaType.Cycle,
  id: 'my first cycle'
};

(async () => {
  const { success, errors } = await schemaValidation(node);
  console.log(success);
  // list of errors in Ajv formatting. See https://github.com/ajv-validator/ajv/tree/v6.12.6#validation-errors
  console.log(errors);
})();
```
