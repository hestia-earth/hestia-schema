import { Ajv, SchemaValidateFunction } from 'ajv';

const getByKey = (prev, curr) =>
  !curr || !prev
    ? prev
    : Array.isArray(prev)
    ? prev.map(item => getItemValue(item, curr))
    : prev
    ? prev[curr]
    : undefined;

const getItemValue = (item: any, key: string) => key.split('.').reduce(getByKey, item);

export const keyword = 'uniqueArrayItem';

const findIdenticalIndexes = (values: string[]) => (value: string, index: number) => ({
  index,
  duplicatedIndexes: values
    .map((otherValue, otherIndex) => [otherValue, otherIndex])
    .filter(([otherValue, otherIndex]) => otherIndex !== index && otherValue === value)
    .map(([_otherValue, otherIndex]) => otherIndex)
});

/**
 * Make sure empty JSON are not set as `'{}'`.
 *
 * @param value
 * @returns
 */
const toString = (value: any) => Object.values(value).filter(Boolean).length ? JSON.stringify(value) : null;

export const validate: SchemaValidateFunction = (schema: string[], data: any[], _parentSchema?: any, dataPath = '') => {
  const values = data.map(item =>
    toString(
      schema.reduce(
        (prev, key) => ({
          ...prev,
          [key]: getItemValue(item, key)
        }),
        {}
      )
    )
  ).filter(Boolean);
  const indexes = values
    .map(findIdenticalIndexes(values))
    .filter(({ duplicatedIndexes }) => duplicatedIndexes.length > 0)
    .sort();

  validate.errors = indexes.map(({ index, duplicatedIndexes }) => ({
    dataPath: `${dataPath}[${index}]`,
    keyword,
    schemaPath: '#/invalid',
    message: 'every item in the list should be unique',
    params: { keyword, keys: schema, duplicatedIndexes }
  }));
  return indexes.length === 0;
};

export const init = (ajv: Ajv) =>
  ajv.addKeyword(keyword, {
    type: 'array',
    errors: 'full',
    validate
  });
