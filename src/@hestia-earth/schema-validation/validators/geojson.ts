import { Ajv, SchemaValidateFunction } from 'ajv';
import * as gjv from 'geojson-validation';

export const keyword = 'geojson';

const invalidCoordinatesError = 'invalid coordinates';
const maxLongitude = 180;
const maxLatitude = 90;

gjv.define(
  'Position',
  ([longitude, latitude]) =>
    [longitude < maxLongitude, longitude > -maxLongitude, latitude < maxLatitude, latitude > -maxLatitude].every(
      Boolean
    ) || [invalidCoordinatesError]
);

export const validate: SchemaValidateFunction = (
  _schema: any,
  value: object,
  _parentSchema: object,
  dataPath: string
) => {
  const validationErrors = gjv.valid(value, true) as string[];
  const isInvalidCoordinates = validationErrors.every(error => error.includes(invalidCoordinatesError));
  const errors = [
    validationErrors.length === 0
      ? null
      : {
          dataPath,
          keyword,
          schemaPath: '#/invalid',
          message: 'not a valid GeoJSON',
          params: {
            ...(isInvalidCoordinates ? { invalidCoordinates: true } : {})
          }
        }
  ].filter(Boolean);
  validate.errors = errors;
  return validationErrors.length === 0;
};

export const init = (ajv: Ajv) =>
  ajv.addKeyword(keyword, {
    type: 'object',
    errors: 'full',
    validate
  });
