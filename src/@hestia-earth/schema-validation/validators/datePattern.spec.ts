import { expect } from 'chai';
import 'mocha';

import { keyword, validate, isValidDate } from './datePattern';

describe('schema-validation/validators/datePattern', () => {
  describe('isValidDate', () => {
    it('should validate dates', () => {
      expect(isValidDate('2016-09-30')).to.equal(true);
      expect(isValidDate('2012')).to.equal(true);
      expect(isValidDate('2012-01')).to.equal(true);
      expect(isValidDate('2012-01-01T00:00:00.00Z')).to.equal(true);
      // not in the calendar
      expect(isValidDate('2016-09-31')).to.equal(false);
      expect(isValidDate('2012-14')).to.equal(false);
    });
  });

  describe('validate', () => {
    const dataPath = '.key';

    describe('with valid date', () => {
      const date = '2012-12-31';

      it('should return true', () => {
        expect(validate({}, date, {}, dataPath)).to.equal(true);
      });
    });

    describe('with invalid date', () => {
      const date = '2012-13-01';

      it('should return false', () => {
        expect(validate({}, date, {}, dataPath)).to.equal(false);
      });
    });

    describe('with an array', () => {
      const dates = ['2012-12-31', '2012-13-01', '--13-01', '--12'];

      it('should validate each value', () => {
        validate({}, dates, {}, dataPath) as boolean;

        expect((validate as any).errors).to.deep.equal([
          {
            dataPath: '.key[1]',
            keyword,
            schemaPath: '#/invalid',
            message: 'not a valid date',
            params: {}
          },
          {
            dataPath: '.key[2]',
            keyword,
            schemaPath: '#/invalid',
            message: 'not a valid date',
            params: {}
          }
        ]);
      });
    });
  });
});
