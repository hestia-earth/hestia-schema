import { expect } from 'chai';
import 'mocha';

import { keyword, validate } from './geojson';

describe('schema-validation/validators/geojson', () => {
  describe('validate', () => {
    const dataPath = '.key';

    describe('with valid GeoJSON', () => {
      const data = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: null,
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [40.419690379341276, -3.734204806574022],
                  [40.42039282621048, -3.7320912258702865],
                  [40.42031931467245, -3.7298918144780746],
                  [40.41942900188972, -3.7304497139531723],
                  [40.419690379341276, -3.734204806574022]
                ]
              ]
            }
          }
        ]
      };

      it('should return true', () => {
        expect(validate({}, data, {}, dataPath)).to.equal(true);
      });
    });

    describe('with invalid GeoJSON', () => {
      const data = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: null,
            geometry: {
              type: 'Polygon',
              coordinates: [
                [40.419690379341276, -3.734204806574022],
                [40.42039282621048, -3.7320912258702865],
                [40.42031931467245, -3.7298918144780746],
                [40.41942900188972, -3.7304497139531723]
              ]
            }
          }
        ]
      };

      it('should return false', () => {
        expect(validate({}, data, {}, dataPath)).to.equal(false);

        expect(validate.errors).to.deep.equal([
          {
            dataPath: '.key',
            keyword,
            schemaPath: '#/invalid',
            message: 'not a valid GeoJSON',
            params: {}
          }
        ]);
      });
    });

    describe('with invalid coordinates', () => {
      const data = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: null,
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [266396.119099999777973, 97677.579800000414252],
                  [266393.154400000348687, 97683.573999999091029],
                  [266388.511800000444055, 97693.593900000676513],
                  [266409.418999999761581, 97682.479599999263883],
                  [266397.556200000457466, 97678.371400000527501],
                  [266396.119099999777973, 97677.579800000414252]
                ]
              ]
            }
          }
        ]
      };

      it('should return false', () => {
        expect(validate({}, data, {}, dataPath)).to.equal(false);

        expect(validate.errors).to.deep.equal([
          {
            dataPath: '.key',
            keyword,
            schemaPath: '#/invalid',
            message: 'not a valid GeoJSON',
            params: {
              invalidCoordinates: true
            }
          }
        ]);
      });
    });
  });
});
