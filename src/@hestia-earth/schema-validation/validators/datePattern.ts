import { Ajv, SchemaValidateFunction } from 'ajv';

/**
 * JavaScript does a lenient parsing of dates, so even `2000-30-30` will return a date.
 * This makes sure the parse date is the date as the given date. *
 *
 * @param value
 * @returns
 */
const isValidDateRange = (value: string) => {
  const [year, month, day] = value.substring(0, 10).split('-').map(Number);
  const date = new Date(year, month - 1, day, 12); // Months are 0-indexed
  return date.getFullYear() === year && date.getMonth() === month - 1 && date.getDate() === day;
};

export const isValidDate = (date: string) =>
  isFinite(new Date(date).getTime()) && (date.length < 10 || isValidDateRange(date));

export const keyword = 'datePattern';

export const validate: SchemaValidateFunction = (
  _schema: any,
  value: string | string[],
  _parentSchema: object,
  dataPath: string
) => {
  const values = Array.isArray(value) ? value : [value];
  const errors = values
    .map((v, index) =>
      isValidDate(v)
        ? null
        : {
            dataPath: `${dataPath}${Array.isArray(value) ? `[${index}]` : ''}`,
            keyword,
            schemaPath: '#/invalid',
            message: 'not a valid date',
            params: {}
          }
    )
    .filter(Boolean);
  validate.errors = errors;
  return errors.length === 0;
};

export const init = (ajv: Ajv) =>
  ajv.addKeyword(keyword, {
    type: ['string', 'array'],
    errors: 'full',
    validate
  });
