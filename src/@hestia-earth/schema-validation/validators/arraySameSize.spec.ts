import { expect } from 'chai';
import 'mocha';

import { validate } from './arraySameSize';

describe('schema-validation/validate', () => {
  describe('validate', () => {
    const dataPath = '.key';
    const keys = ['value'];
    const items = [0, 10, 250];

    describe('with valid item', () => {
      const item = {
        value: [1, 20, 30]
      };

      it('should return true', () => {
        expect(validate(keys, items, {}, dataPath, item)).to.equal(true);
      });
    });

    describe('with invalid items', () => {
      const item = {
        value: [1, 20]
      };

      it('should return false', () => {
        expect(validate(keys, items, {}, dataPath, item)).to.equal(false);
      });
    });
  });
});
