import { NodeType } from '@hestia-earth/schema';
import { definitions, loadSchemas } from '@hestia-earth/json-schema';
import { isCSVIncluded, schemaFromKey } from '@hestia-earth/json-schema/schema-utils';
const get = require('lodash.get');

import { isExpandable } from './utils';

const noValue = '-';
export const csvColSep = ',';

const ignoreValues: {
  [key: string]: (value: any) => boolean;
} = {
  Date: val => val instanceof Date,
  GeoJSON: val => typeof val === 'object' && ('features' in val || 'feature' in val || 'geometry' in val)
};

const isIgnored = (val: any) => Object.values(ignoreValues).some(func => func(val));

/**
 * Get the headers in a CSV file.
 *
 * @param csv The CSV content as string.
 * @returns headers as a list.
 */
export const headersFromCsv = (csv: string) => csv.split('\n')[0].split(csvColSep);

const convertValue = {
  default: (value: any) => `${value}`,
  object: (value: object) => (value === null ? noValue : value instanceof Date ? value.toJSON() : JSON.stringify(value))
};

const escapeField = (value: string) => {
  const convertType = typeof value in convertValue ? typeof value : 'default';
  const val = `${convertValue[convertType](value)}`.replace(/"/g, '""').replace(/\r/g, '').replace('\n', '');
  return val.includes(csvColSep) ? `"${val}"` : val;
};

const skipColumns = ['type', '@type', '@context'];

const typeToColumnName = (type = '') => `${type.substring(0, 1).toLowerCase()}${type.substring(1)}`;

const getChildrenHeaders = (key: string, value, useBrackets: boolean) => {
  const headers = getHeaders(value, useBrackets);
  return Array.isArray(value)
    ? headers.map(header => {
        const [index, ...rest] = header.split('.');
        return `${key}${useBrackets ? `[${index}]` : `.${index}`}.${rest.join('.')}`;
      })
    : headers.map(header => `${key}.${header}`);
};

const getHeaders = (node, useBrackets: boolean) =>
  Object.keys(node)
    .flatMap(key => {
      const value = node[key];
      return isExpandable(value) && !isIgnored(value) ? getChildrenHeaders(key, value, useBrackets) : key;
    })
    .filter(key => !skipColumns.includes(key));

const isIndexedNode = (headers: string[]) => {
  const topLevel = headers.filter(header => !header.includes('.'));
  return topLevel.some(header => header === '@id');
};

export interface IConvertCSVOptions {
  /**
   * Include top-level existing nodes. Defaults to `true`.
   */
  includeExising?: boolean;
  /**
   * Use `[0]` notation for arrays instead of `.0`. Defaults to `false`.
   */
  useBrackets?: boolean;
  /**
   * By default, internal properties are not included. Set to `true` to include them.
   */
  includeAllHeaders?: boolean;
  /**
   * Override list of headers in order. By default, compile all from nodes and sort as they appear.
   */
  selectedHeaders?: string[];
}

const defaultOptions: IConvertCSVOptions = {
  includeExising: true,
  useBrackets: false,
  includeAllHeaders: false,
  selectedHeaders: []
};

const filterTermHeaders = (schemas: definitions, header: string) => {
  const { title } = schemaFromKey(schemas, header);
  return title !== NodeType.Term || header.endsWith('id') || header.endsWith('name');
};

const filterHeaders = (headers: string[]) => {
  const schemas = loadSchemas();
  const filterIncluded = isCSVIncluded(schemas);
  return headers.filter(header => filterIncluded(header) && filterTermHeaders(schemas, header));
};

/**
 * Convert a list of Nodes to CSV format.
 *
 * @param nodes List of nodes to convert.
 * @param options Conversion options.
 * @returns CSV content formatted for upload on Hestia.
 */
export const toCsv = (nodes: any[], options?: IConvertCSVOptions) => {
  const { includeExising, useBrackets, includeAllHeaders, selectedHeaders } = { ...defaultOptions, ...options };

  const headersByType = nodes.reduce(
    (prev, { type, '@type': _t, ...node }) => {
      type = type || _t;
      prev[type] = type in prev ? prev[type] : [];
      const headers = getHeaders(node, useBrackets);

      prev[type] = Array.from(new Set([...prev[type], ...(!includeExising && isIndexedNode(headers) ? [] : headers)]));
      return prev;
    },
    {} as { [type: string]: string[] }
  );
  const headers = Object.keys(headersByType).flatMap(type =>
    headersByType[type].map(key => `${typeToColumnName(type)}.${key}`)
  );
  const allHeaders = selectedHeaders.length ? selectedHeaders : includeAllHeaders ? headers : filterHeaders(headers);

  return [
    allHeaders.join(csvColSep),
    nodes
      // remove all top-level nodes that have an @id - already uploaded
      .filter(node => includeExising || !node['@id'])
      .map(node =>
        allHeaders
          .map(header => {
            const [type, ...rest] = header.split('.');
            const ntype = node.type || node['@type'];
            const value = typeToColumnName(ntype) === type ? get(node, rest.join('.'), noValue) : noValue;
            return Array.isArray(value) ? escapeField(value.join(';')) : escapeField(value);
          })
          .join(csvColSep)
      )
      .join('\n')
  ].join('\n');
};
