import { expect } from 'chai';
import 'mocha';
import { SchemaType } from '@hestia-earth/schema';

import { readFileAsString } from '../../../test/utils';
import { loadSchemas } from '../json-schema';
import { pivotNode, pivotNodes } from './json-pivot';

const fixturesFolder = 'schema-convert/json-pivot';

const readFixture = (filename: string) => readFileAsString(`${fixturesFolder}/${filename}`);

describe('schema-convert/json-pivot', () => {
  const schemas = loadSchemas();

  describe('pivotNode', () => {
    it('should handle a cycle', () => {
      const filekey = 'cycle';

      const node = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNode(schemas, node);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle a complex cycle', () => {
      const filekey = 'cycle1';

      const node = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNode(schemas, node);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle nested blank nodes', () => {
      const filekey = 'nested-blank-nodes';

      const node = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNode(schemas, node);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle an impact', () => {
      const filekey = 'impact';

      const node = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNode(schemas, node);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle deep nodes', () => {
      const filekey = 'deep';

      const node = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNode(schemas, node);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle unindexed nodes', () => {
      const filekey = 'unindexed-node';

      const node = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNode(schemas, node);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle merging array fields', () => {
      const filekey = 'node-arrayfields-merged';

      const node = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNode(schemas, node);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle non-existing node', () => {
      const node = { type: SchemaType.Cycle, id: 'cycle' };
      expect(pivotNode(schemas, node)).to.deep.equal(node);
    });
  });

  describe('pivotNodes', () => {
    it('should handle properties', () => {
      const filekey = 'properties';

      const nodes = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNodes(schemas, nodes);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });

    it('should handle null values', () => {
      const filekey = 'null-values';

      const nodes = JSON.parse(readFixture(`${filekey}.json`));
      const result = pivotNodes(schemas, nodes);

      const expected = JSON.parse(readFixture(`${filekey}-pivoted.json`));
      expect(result).to.deep.equal(expected);
    });
  });
});
