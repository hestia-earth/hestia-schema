import { expect } from 'chai';
import 'mocha';

import { readFileAsString } from '../../../test/utils';
import { toCsv } from './csv';

const fixturesFolder = 'schema-convert/csv';

describe('schema-convert/csv', () => {
  describe('toCsv', () => {
    const boundary = { type: 'Feature', geometry: { type: 'Polygon' } };
    const { nodes } = JSON.parse(readFileAsString(`${fixturesFolder}/nodes.json`));

    it('should filter by headers', () => {
      const result = toCsv(nodes, {
        includeExising: false,
        selectedHeaders: ['cycle.id', 'cycle.defaultSource.id', 'cycle.inputs.0.value']
      });
      const expected = readFileAsString(`${fixturesFolder}/filtered-headers.csv`);
      expect(result).to.equal(expected);
    });

    it('should handle GeoJSON as string', () => {
      const node = {
        type: 'Organisation',
        boundary: JSON.stringify(boundary)
      };
      const result = toCsv([node]);
      const expected = readFileAsString(`${fixturesFolder}/geojson.csv`);
      expect(result).to.equal(expected);
    });

    it('should handle GeoJSON as object', () => {
      const node = {
        type: 'Organisation',
        boundary
      };
      const result = toCsv([node]);
      const expected = readFileAsString(`${fixturesFolder}/geojson.csv`);
      expect(result).to.equal(expected);
    });

    it('should convert dates', () => {
      const node = JSON.parse(readFileAsString(`${fixturesFolder}/convert-date.json`));
      const result = toCsv([node], { includeAllHeaders: true });
      const expected = readFileAsString(`${fixturesFolder}/convert-date.csv`);
      expect(result).to.equal(expected);
    });

    describe('including existing nodes and all headers', () => {
      it('should return csv as text', () => {
        const result = toCsv(nodes, { includeExising: true, includeAllHeaders: true });
        const expected = readFileAsString(`${fixturesFolder}/existing-nodes-all-headers.csv`);
        expect(result).to.equal(expected);
      });
    });

    describe('including existing nodes not all headers', () => {
      it('should return csv as text', () => {
        const result = toCsv(nodes, { includeExising: true, includeAllHeaders: false });
        const expected = readFileAsString(`${fixturesFolder}/existing-nodes.csv`);
        expect(result).to.equal(expected);
      });
    });

    describe('not including existing nodes', () => {
      it('should return csv as text', () => {
        const result = toCsv(nodes, { includeExising: false, useBrackets: true, includeAllHeaders: true });
        const expected = readFileAsString(`${fixturesFolder}/no-existing-nodes.csv`);
        expect(result).to.equal(expected);
      });
    });

    describe('with terms', () => {
      const node = JSON.parse(readFileAsString(`${fixturesFolder}/with-terms.json`));

      it('should return csv as text', () => {
        const result = toCsv([node], { includeAllHeaders: false });
        const expected = readFileAsString(`${fixturesFolder}/with-terms.csv`);
        expect(result).to.equal(expected);
      });

      describe('with all headers', () => {
        it('should return csv as text', () => {
          const result = toCsv([node], { includeAllHeaders: true });
          const expected = readFileAsString(`${fixturesFolder}/with-terms-all-headers.csv`);
          expect(result).to.equal(expected);
        });
      });
    });
  });
});
