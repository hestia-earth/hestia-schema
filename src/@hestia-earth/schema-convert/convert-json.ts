import { promises } from 'fs';
import { loadSchemas, definitions } from '@hestia-earth/json-schema';

import { listFiles } from './file-utils';
import { toJson } from './json';

const [path] = process.argv.slice(2);

const { readFile, writeFile } = promises;
const encoding = 'utf8';
const ext = '.csv';

const processFile = async (schemas: definitions, filepath: string) => {
  const content = await readFile(filepath, encoding);
  const nodes = await toJson(schemas, content);
  const destPath = filepath.replace(ext, '.json');
  await writeFile(destPath, JSON.stringify({ nodes }, null, 2), encoding);
  return destPath;
};

export const run = async () => {
  const schemas = loadSchemas();
  const files = await listFiles(path, ext);
  return Promise.all(files.map(file => processFile(schemas, file)));
};
