import { definitions } from '@hestia-earth/json-schema/types';

import { convertValue, omitKeys, jsonToCsv, uncapitalize } from './utils';
import { pivotNode } from './json-pivot';

const parseValue = (value: any) => {
  const result = convertValue(value);
  return typeof result === undefined || result === null ? '' : result.toString();
};

const withType = (nodes: any[]) => nodes.map(node => ({ [uncapitalize(node['@type'] || node.type)]: node }));

export const toCsv = (nodes: any[], keys?: string[], excludeKeys = omitKeys) =>
  jsonToCsv(withType(nodes), {
    keys,
    excludeKeys,
    emptyFieldValue: '-',
    parseValue
  });

/**
 * CSV format for data processing.
 *
 * @pram schemas Schema definitions to use for pivoting.
 * @param nodes List of nodes to convert.
 * @returns CSV content formatted with pivoted blank nodes.
 */
export const toCsvPivot = (schemas: definitions, nodes: any[]) => toCsv(nodes.map(node => pivotNode(schemas, node)));
