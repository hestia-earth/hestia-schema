import { expect } from 'chai';
import 'mocha';

import { daysInYear } from './utils';

describe('schema-convert/utils', () => {
  describe('daysInYear', () => {
    it('should handle leap years', () => {
      expect(daysInYear(2000)).to.equal(366);
      expect(daysInYear(2004)).to.equal(366);
      expect(daysInYear(2020)).to.equal(366);
    });

    it('should handle normal years', () => {
      expect(daysInYear(2001)).to.equal(365);
      expect(daysInYear(2021)).to.equal(365);
    });
  });
});
