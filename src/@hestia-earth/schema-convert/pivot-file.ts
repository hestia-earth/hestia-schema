import { promises } from 'fs';
import { extname } from 'path';
import { loadSchemas, definitions } from '@hestia-earth/json-schema';

import { listFiles } from './file-utils';
import { INode, pivotNode, pivotNodes } from './json-pivot';
import { toCsv } from './csv-pivot';

const [path, format] = process.argv.slice(2);

const { readFile, writeFile } = promises;
const encoding = 'utf8';

enum Format {
  csv = 'csv',
  json = 'json'
}
const formats = Object.values(Format);

if (format && !formats.includes(format as Format)) {
  throw new Error(`Invalid format. Please use either ${formats.map(v => `"${v}"`).join(' or ')}`);
}

const saveToFileExt: {
  [format in Format]: (filepath: string, nodes: INode[]) => Promise<void>;
} = {
  json: (filepath, nodes) =>
    writeFile(filepath.replace(extname(filepath), '.json'), JSON.stringify(nodes, null, 2), encoding),
  csv: (filepath, nodes) => writeFile(filepath.replace(extname(filepath), '.csv'), toCsv(nodes), encoding)
};

const processJsonFile = async (schemas: definitions, filepath: string) => {
  const data = JSON.parse(await readFile(filepath, encoding));
  const pivoted = Array.isArray(data)
    ? pivotNodes(schemas, data)
    : 'nodes' in data
    ? { nodes: pivotNodes(schemas, data.nodes) }
    : pivotNode(schemas, data);
  return saveToFileExt[format](filepath, pivoted);
};

const processCsvFile = async (schemas: definitions, filepath: string) => {
  const { toJson } = await import('./json');
  const content = await readFile(filepath, encoding);
  const nodes = await toJson(schemas, content, { ignoreInternal: false, addDefaults: false });
  const pivoted = pivotNodes(schemas, nodes);
  return saveToFileExt[format](filepath, pivoted);
};

const processFileExt = {
  '.json': processJsonFile,
  '.jsonld': processJsonFile,
  '.csv': processCsvFile
};

export const run = async () => {
  const schemas = loadSchemas();
  const files = await listFiles(path, '.csv', '.jsonld', '.json');
  return Promise.all(files.map(file => processFileExt[extname(file)](schemas, file)));
};
