# HESTIA Schema Converter

Module to convert CSV to JSON or JSON to CSV following HESTIA's Schema

## Install

```sh
npm install @hestia-earth/schema-convert
```

## Usage

### Converting CSV to JSON

```sh
hestia-convert-to-json <folder containing .csv files>
```

### Converting JSON to CSV

```sh
hestia-convert-to-csv <folder containing .json or .jsonld files>
```
