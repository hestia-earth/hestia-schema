import {
  SchemaType,
  NodeType,
  uniquenessFields,
  isBlankNode,
  isNode,
  termFields,
  refToSchemaType
} from '@hestia-earth/schema';
import {
  definitions,
  IDefinitionObject,
  IDefinitionArray,
  definition,
  genericType
} from '@hestia-earth/json-schema/types';
import { keyType } from '@hestia-earth/json-schema/schema-utils';
const get = require('lodash.get');

import { childkey, findNonDuplicates, omit, parentKey } from './utils';

export interface INode {
  type?: SchemaType;
  '@type'?: NodeType;
  [key: string]: any;
}

type baseValue = string | boolean | number;

const ignoreKeys = [
  '@context',
  '@type',
  'type',
  'added',
  'addedVersion',
  'updated',
  'updatedVersion',
  '_cache',
  // ignore all term keys as only the `@id` is included in the key
  'term',
  ...termFields.map(v => `term.${v}`)
];

const headerJoin = '+';
const headerParentKey = (key: string) => key.split(headerJoin)[0];

const uniqueData = (value: Record<string, any>, key: string) => {
  // in some cases, we are getting a list using "inputs.@id", but `get` will not be able to access this
  const parentData = key.includes('.') ? get(value, parentKey(key), undefined) : undefined;
  return Array.isArray(parentData) ? parentData.map(d => uniqueData(d, childkey(key))) : get(value, key, undefined);
};

const valueAsString = (value: baseValue | baseValue[] | object) =>
  (Array.isArray(value) ? value : [value]).map(v => `${v}`).join(';');

const valueAsKey = (value: string) => ([value.match(/[\s\"\'\.;]/g)].some(Boolean) ? `[${value}]` : value);

const pivotValue = (schemas: definitions, nodeType: SchemaType | NodeType, key: string, value: Record<string, any>) => {
  // nodeType = Cycle
  // key = emissions
  // value = {'term':{'@id':'gwp100}, 'dates':['2021','2022'], 'value':[10,12]}
  const uniqueFields: string[] = uniquenessFields[nodeType]?.[key] ?? [];
  // uniqueFields = ['term.@id', 'dates']
  const pivotedKey = uniqueFields.length
    ? uniqueFields
        .map(field => {
          const data = uniqueData(value, field);
          return typeof data === 'undefined'
            ? undefined
            : field === 'term.@id' || uniqueFields.length === 1
            ? valueAsKey(valueAsString(data))
            : `${parentKey(field)}[${valueAsString(data)}]`;
        })
        .filter(Boolean)
        .join(headerJoin)
    : '';
  // pivotedKey = 'gwp100+dates[2021;2022]'
  const data = omit(pivotNode(schemas, value, false), [...uniqueFields, ...ignoreKeys]);
  return { [pivotedKey]: data };
};

const simplifiedKeys = (keys: string[]) => findNonDuplicates(keys.map(headerParentKey));

const mergeUnpivotedBlankNode = (values: string[], existingValue?: any) =>
  Object.assign({}, ...values.map(v => ({ [v]: {} })), existingValue || {});

const mergeUnpivotedNodes = (values: string[], uniqueArrayKey: string) => values.map(v => ({ [uniqueArrayKey]: v }));

const mergeUnpivotedByType: {
  [type: string]: (values: string[], definition?: definition, uniqueArrayKey?: string, existingValue?: any) => any;
} = {
  number: values => Number(values[0]),
  string: values => values[0],
  boolean: values => Boolean(values[0]),
  null: () => null,
  array: (values, definition: IDefinitionArray, uniqueArrayKey, existingValue) =>
    (definition?.items as IDefinitionObject)?.$ref
      ? mergeUnpivotedByType.$ref(values, definition.items, uniqueArrayKey, existingValue)
      : [...(existingValue || []), ...values]
          .map(v => mergeUnpivotedByType[(definition.items.type as genericType) || 'string']([v]))
          .join(';'),
  $ref: (values, { $ref }: IDefinitionObject, uniqueArrayKey, existingValue) =>
    isBlankNode({ type: refToSchemaType($ref) })
      ? mergeUnpivotedBlankNode(values, existingValue)
      : mergeUnpivotedNodes(values, uniqueArrayKey)
};

const mergeUnpivotedValue = (
  definition: IDefinitionObject,
  uniqueArrayItem: string[],
  data: any,
  nodeValue: Record<string, any>,
  pivotedKey: string
) => {
  const [key, newValues] = [pivotedKey.split('[')[0], pivotedKey.split('[')[1].replace(']', '').split(';')];
  const newValueDefinition = definition?.properties?.[key];
  const uniqueArrayKey = childkey((uniqueArrayItem ?? []).find(v => v.startsWith(key)));
  const valueType = newValueDefinition.type;
  const functionValueType = '$ref' in newValueDefinition ? '$ref' : Array.isArray(valueType) ? valueType[0] : valueType;

  data[key] = mergeUnpivotedByType[functionValueType](
    newValues,
    newValueDefinition as IDefinitionArray,
    uniqueArrayKey,
    nodeValue[key]
  );
  return data;
};

const unpivotBlankNode = (
  definition: IDefinitionObject,
  uniqueArrayItem: string[],
  key: string,
  value: Record<string, any>
) => ({
  ...value,
  ...key
    .split(headerJoin)
    .slice(1)
    .reduce((prev, curr) => mergeUnpivotedValue(definition, uniqueArrayItem, prev, value, curr), {})
});

/**
 * Pivot list of Blank Node and simplify when only one instance is found
 *
 * @param nodeType
 * @param key
 * @param value
 */
const pivotBlankNodes = (
  schemas: definitions,
  nodeType: SchemaType | NodeType,
  key: string,
  value: Record<string, any>[]
) => {
  const uniqueFields: string[] = uniquenessFields[nodeType]?.[key] ?? [];
  const data = Object.assign({}, ...value.map(val => pivotValue(schemas, nodeType, key, val)));
  const dataKeys = Object.keys(data);
  const uniqueKeys = simplifiedKeys(dataKeys);
  return uniqueFields?.[0] === 'term.@id'
    ? Object.fromEntries(
        Object.entries(data).map(([entryKey, value]) => {
          const isUnique = entryKey !== headerParentKey(entryKey) && uniqueKeys.includes(headerParentKey(entryKey));
          const blankNodeType = keyType(schemas[nodeType], key);
          return [
            isUnique ? headerParentKey(entryKey) : entryKey,
            isUnique
              ? unpivotBlankNode(
                  schemas[blankNodeType],
                  schemas[nodeType]?.properties?.[key]?.uniqueArrayItem ?? [],
                  entryKey,
                  value
                )
              : value
          ];
        })
      )
    : data;
};

/**
 * Pivot node and return in compacted format.
 *
 * @param schemas The definitions of the HESTIA Schema (`import { loadSchemas } from "@hestia-earth/json-schema"`)
 * @param node The node to pivot.
 * @param keepType To keep the `@type`/`type` key in the pivoted format.
 * @returns
 */
export const pivotNode = (schemas: definitions, { '@type': _type, type, ...node }: INode, keepType = true): INode =>
  Object.fromEntries(
    [
      keepType && (_type || type) ? [_type ? '@type' : 'type', _type || type] : null,
      ...Object.entries(node)
        .filter(
          ([key]) => !ignoreKeys.includes(key) && ((_type || type) !== NodeType.Term || !termFields.includes(key))
        )
        .map(([key, value]) => [
          key,
          value === null
            ? null
            : typeof value === 'object'
            ? Array.isArray(value)
              ? value.every(isNode)
                ? value.map(v => pivotNode(schemas, v, false))
                : value.every(isBlankNode)
                ? pivotBlankNodes(schemas, _type || type, key, value)
                : valueAsString(value)
              : pivotNode(schemas, value, false) // deep pivoting
            : valueAsString(value)
        ])
    ].filter(Boolean)
  );

export const pivotNodes = (schemas: definitions, nodes: any[]) => {
  return nodes.map(node => {
    try {
      return pivotNode(schemas, node);
    } catch (err) {
      throw new Error(
        `Error pivoting ${node['@type'] || node.type} with id "${node['@id'] || node.id}":\n${err.toString()}`
      );
    }
  });
};
