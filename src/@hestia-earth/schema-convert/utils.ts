import { json2csv, Json2CsvOptions } from 'json-2-csv';
const get = require('lodash.get');
const unset = require('lodash.unset');

export const omitKeys = [
  '@context',
  'schemaVersion',
  'dataPrivate',
  ...['bibliography', 'site', 'cycle', 'impactAssessment', 'defaultSource', 'source'].flatMap(k => [
    `${k}.type`,
    `${k}.@type`
  ])
];

export const parentKey = (key: string) => key.split('.')[0];
export const childkey = (key: string) => key.split('.').slice(1).join('.');

export const omit = (data: any, keys: string[]) => {
  const obj = { ...data };

  keys.forEach(key => {
    unset(obj, key);

    // handle arrays and notation without array
    const _parentKey = parentKey(key);
    const _childkey = childkey(key);

    const parentData = key.includes('.') ? get(obj, _parentKey, undefined) : undefined;
    if (typeof parentData === 'object') {
      if (Array.isArray(parentData)) {
        parentData.forEach((_v, index) => {
          unset(obj, `${_parentKey}[${index}].${_childkey}`);
        });

        // only keep values that are not empty
        obj[_parentKey] = obj[_parentKey].filter(v => !isEmpty(v));
      }

      if (isEmpty(obj[_parentKey])) {
        unset(obj, _parentKey);
      }
    }
  });
  return obj;
};

export const isExpandable = (val: any) =>
  !!val && typeof val === 'object' && (Array.isArray(val) ? val.every(isExpandable) : Object.keys(val).length);

export const isIri = (value?: string) => (value || '').startsWith('http');

export const isBoolean = (value: string) => value.toLowerCase() === 'true' || value.toLowerCase() === 'false';

export const isNumber = (n: string | number) => !isNaN(parseFloat(`${n}`)) && isFinite(parseFloat(`${n}`));

const ignoreKey = (key: string) => !['@type', 'type'].includes(key);

export const isEmpty = (value: any, minKeys = 1) =>
  value === null ||
  typeof value === 'undefined' ||
  (typeof value === 'object'
    ? Array.isArray(value)
      ? !value.length
      : Object.keys(value).filter(key => key !== 'type').length < minKeys
    : value === '');

export const nonEmptyValue = (value: any) => !isEmpty(value) && value !== '-';

export const nonEmptyNode = (node: any) =>
  typeof node === 'object'
    ? Object.keys(node).filter(key => ignoreKey(key) && !isEmpty(node[key])).length > 0
    : nonEmptyValue(node);

const isUndefined = <T>(value: T, allowNull: boolean) =>
  (value === null && !allowNull) ||
  typeof value === 'undefined' ||
  (typeof value === 'object' && value !== null && !(value instanceof Date) && !Object.keys(value).length);

export const reduceUndefinedValues = <T>(obj: T, allowNull = false) =>
  Object.keys(obj).reduce((prev, key) => {
    const value = obj[key];
    return { ...prev, ...(isUndefined(value, allowNull) ? {} : { [key]: value }) };
  }, {} as Partial<T>);

export const ellipsis = (text = '', maxlength = 20) =>
  text.length > maxlength ? `${text.substring(0, maxlength)}...` : text;

export const keyToLabel = (key: string) =>
  `${key[0].toUpperCase()}${key
    .replace(/([a-z])([A-Z])/g, '$1 $2')
    .replace(/([_])([a-zA-Z])/g, g => ` ${g[1].toUpperCase()}`)
    .substring(1)}`;

export const diffInDays = (date1: Date | string, date2: Date | string) =>
  Math.abs(Math.round((new Date(date2).getTime() - new Date(date1).getTime()) / (24 * 60 * 60 * 1000)));

export const daysBefore = (date = new Date(), days = 1) => {
  const newDate = new Date(date);
  newDate.setDate(newDate.getDate() - days);
  return newDate;
};

export const daysInYear = (year: number) => ((year % 4 === 0 && year % 100 > 0) || year % 400 === 0 ? 366 : 365);

export const convertValue = (value: any) =>
  Array.isArray(value) ? value.map(String).join(';') : typeof value === 'object' ? JSON.stringify(value) : value;

type counter = { [x: string]: number };

const count = (values: string[]) => values.reduce((a, b) => ({ ...a, [b]: (a[b] || 0) + 1 }), {} as counter);

const duplicates = (dict: counter) => Object.keys(dict).filter(a => dict[a] > 1);

const nonDuplicates = (dict: counter) => Object.keys(dict).filter(a => dict[a] === 1);

export const findDuplicates = (values: string[]) => duplicates(count(values));

export const findNonDuplicates = (values: string[]) => nonDuplicates(count(values));

export const uncapitalize = (text: string) => `${text[0].toLowerCase()}${text.substring(1)}`;

/**
 * Move every item on the same row.
 *
 * @param data
 * @returns List of original records with less gaps in data.
 */
const compactDataForCsv = (data: Record<string, any>[]) =>
  data.reduce(
    (prev: Record<string, any>[], curr) => {
      const [[key, value]] = Object.entries(curr);
      const prevNoKeyIndex = prev.findIndex(p => !Object.keys(p).includes(key));
      prevNoKeyIndex >= 0 ? (prev[prevNoKeyIndex][key] = value) : prev.push(curr);
      return prev;
    },
    [] as Record<string, any>[]
  ) as Record<string, any>[];

export const jsonToCsv = (values: Record<string, any>[], options: Json2CsvOptions = {}) =>
  json2csv(compactDataForCsv(values), {
    emptyFieldValue: '',
    expandNestedObjects: true,
    expandArrayObjects: true,
    arrayIndexesAsKeys: true,
    sortHeader: true,
    ...options
  });
