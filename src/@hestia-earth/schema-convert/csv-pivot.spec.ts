import { expect } from 'chai';
import 'mocha';

import { readFileAsString } from '../../../test/utils';
import { loadSchemas } from '../json-schema';
import { toJson } from './json';
import { toCsvPivot } from './csv-pivot';

const fixturesFolder = 'schema-convert/csv-pivot';

const readFixture = (filename: string) => readFileAsString(`${fixturesFolder}/${filename}`);

describe('schema-convert/csv-pivot', () => {
  const schemas = loadSchemas();

  describe('toCsvPivot', () => {
    const expected = readFixture('pivoted.csv');

    it('should convert from JSON', () => {
      const nodes = JSON.parse(readFixture('original.json'));
      const result = toCsvPivot(schemas, nodes);
      expect(expected.trim()).to.equal(result);
    });

    it('should convert nodes for upload', () => {
      const nodes = JSON.parse(readFixture('upload.json'));
      const result = toCsvPivot(schemas, nodes);
      expect(readFixture('upload.csv').trim()).to.equal(result);
    });

    it('should convert from CSV', async () => {
      const original = readFixture('original.csv');
      const nodes = await toJson(loadSchemas(), original, { ignoreInternal: false, addDefaults: false });
      const result = toCsvPivot(schemas, nodes);
      expect(expected.trim()).to.equal(result);
    });
  });
});
