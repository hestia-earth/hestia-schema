import { promises } from 'fs';
import { extname, join } from 'path';

const { lstat, readdir } = promises;

export const listFiles = async (path: string, ...extensions: string[]): Promise<string[]> => {
  const stat = await lstat(path);
  const files = (
    await Promise.all(
      stat.isDirectory() ? (await readdir(path)).map(file => listFiles(join(path, file), ...extensions)) : [path]
    )
  ).flat();
  return files.filter(file => extensions.includes(extname(file)));
};
