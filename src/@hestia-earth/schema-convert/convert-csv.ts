import { promises } from 'fs';
import { loadSchemas, definitions } from '@hestia-earth/json-schema';

import { listFiles } from './file-utils';
import { toCsvPivot } from './csv-pivot';
import { toCsv } from './csv';

const [path] = process.argv.slice(2);

const { readFile, writeFile } = promises;
const encoding = 'utf8';

const processFile = async (schemas: definitions, filepath: string, pivot: boolean) => {
  const data = await readFile(filepath, encoding);
  const content = JSON.parse(data.trim());
  const nodes = Array.isArray(content) ? content : 'nodes' in content ? content.nodes : [content];
  const res = pivot ? toCsvPivot(schemas, nodes) : toCsv(nodes, { includeAllHeaders: true, includeExising: true });
  const destPath = filepath.replace('.jsonld', '.csv').replace('.json', '.csv');
  await writeFile(destPath, res, encoding);
  return destPath;
};

export const run = async (pivot = false) => {
  const schemas = loadSchemas();
  const files = await listFiles(path, '.jsonld', '.json');
  return Promise.all(files.map(file => processFile(schemas, file, pivot)));
};
