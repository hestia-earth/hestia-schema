#!/usr/bin/env node
import { run } from '../pivot-file';

const now = new Date().getTime();

run()
  .then(paths => {
    console.log('Done pivoting', paths.length, 'files in', (new Date().getTime() - now) / 1000, 's');
    console.log(paths.join('\n'));
    process.exit(0);
  })
  .catch(err => {
    console.log('Error pivoting files.');
    console.error(err);
    process.exit(1);
  });
