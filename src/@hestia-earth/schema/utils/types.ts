import { JSON, SchemaType, NodeType } from '../types';
import { schemaTermTypes } from '../termTypes';
import { TermTermType } from '../Term';

export const jsonldPath = (type: SchemaType | NodeType, id: string) => `${type}/${id}.jsonld`;

export const isTypeValid = (node: any) => Object.values(SchemaType).includes(node['@type'] || node.type);

export const isTypeNode = (type: SchemaType | NodeType) => Object.values(NodeType).includes(type as NodeType);

export const isTypeBlankNode = (type: SchemaType) => isTypeValid({ type }) && !isTypeNode(type);

export const isNode = (node: any) => isTypeNode(node['@type'] || node.type);

export const isBlankNode = (node: any) => isTypeBlankNode(node['@type'] || node.type);

export const isExpandable = (val: any) =>
  !!val &&
  !(val instanceof Date) &&
  typeof val === 'object' &&
  (Array.isArray(val) ? val.every(isExpandable) : Object.values(SchemaType).includes(val.type || val['@type']));

export const typeToSchemaType = (type?: string) =>
  Object.values(SchemaType).find((val: SchemaType) => val.toLowerCase() === (type || '').toLowerCase());

export const refToSchemaType = (ref = ''): string | SchemaType =>
  ref
    ? ref.startsWith('http')
      ? ref
      : typeToSchemaType(ref.substring(2).replace('-deep', '').replace('.json#', ''))
    : undefined;

/**
 * Get a list of possible places where the `termType` can be used.
 * @param termType The `termType`.
 * @returns
 */
export const schemaFromTermType = (termType: TermTermType) =>
  schemaTermTypes.flatMap(({ type, mappings }) =>
    Object.entries(mappings)
      .filter(([key, termTypes]) => key !== 'all' && Array.isArray(termTypes) && termTypes.includes(termType))
      .map(([key]) => ({ type, key }))
  );
