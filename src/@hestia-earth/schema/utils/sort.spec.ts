import { expect } from 'chai';
import 'mocha';

import { readFileAsString } from '../../../../test/utils';
import { sortKeysByType } from './sort';

describe('schema/utils/sort', () => {
  describe('sortKeysByType', () => {
    const keys = readFileAsString('schema/utils/headers.csv').trim().split(',');

    it('should sort the keys following the schema', () => {
      expect(sortKeysByType(keys)).to.deep.equal([
        'cycle.id',
        'cycle.inputs.0.term.@id',
        'cycle.inputs.0.term.name',
        'cycle.inputs.0.term.units',
        'cycle.inputs.0.term.defaultProperties.0.term.units',
        'cycle.inputs.0.term.defaultProperties.0.value',
        'cycle.inputs.0.term.termType',
        'cycle.inputs.0.value',
        'cycle.inputs.0.sd',
        'cycle.inputs.0.statsDefinition',
        'cycle.inputs.0.observations',
        'cycle.inputs.1.term.@id',
        'cycle.inputs.1.term.name',
        'cycle.inputs.1.term.units',
        'cycle.inputs.1.term.termType',
        'cycle.inputs.1.value'
      ]);
    });
  });
});
