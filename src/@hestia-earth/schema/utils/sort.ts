import { SchemaType } from '../types';
import { typeToSchemaType } from './types';

const defaultSortConfig = require('../sort-config/config.json');

export type sortConfig = {
  [type in SchemaType]: {
    [key: string]: {
      order: string;
      type: string;
    };
  };
};

const typeSortOrder = (sortConfig: sortConfig, type: SchemaType, key: string) =>
  type in sortConfig && key in sortConfig[type]
    ? sortConfig[type][key]
    : {
        order: Number.MAX_SAFE_INTEGER.toString(),
        type: 'string'
      };

const keyDelimiter = '.';

const isNumber = (key: string) => key.match(/^\d+$/g) !== null;
const keyParts = (key: string) => key.split(keyDelimiter);
const typeLevel = (sortConfig: sortConfig, type: SchemaType) => sortConfig[type].index.order;
const maxLevelByType = (sortConfig: sortConfig, type: any) => sortConfig[type].type.order.length;

const sortOrder =
  (sortConfig: sortConfig, maxNestedLevel = 1) =>
  (fullKey: string, parentType?: string) => {
    const [keyType, key, ...left] = keyParts(fullKey);
    const keyAsNumber = isNumber(key);
    const nestedKey = [key, ...left].join(keyDelimiter);
    const schemaType = typeToSchemaType(parentType || keyType);
    const { order, type } = keyAsNumber
      ? { order: key.padStart(maxLevelByType(sortConfig, parentType), '0'), type: parentType }
      : typeSortOrder(sortConfig, schemaType, key);
    const nestedOrder: string = left.length ? sortOrder(sortConfig, maxNestedLevel)(nestedKey, type) : '';
    const fullOrder = (order + nestedOrder).padEnd(parentType ? 0 : maxNestedLevel * order.length, '0');
    return parentType ? fullOrder : typeLevel(sortConfig, schemaType) + fullOrder;
  };

export const sortKeysByType = (keys: string[], sort: 'asc' | 'desc' = 'asc', sortConfig = defaultSortConfig) => {
  const maxNestedLevel = keys
    .map(key => keyParts(key).length)
    .sort()
    .pop();
  const sorter = sortOrder(sortConfig, maxNestedLevel);
  const orders = keys.reduce((prev, key) => ({ ...prev, [key]: sorter(key) }), {});
  return keys.sort((a, b) =>
    sort === 'asc'
      ? parseInt(orders[a], 10) - parseInt(orders[b], 10)
      : parseInt(orders[b], 10) - parseInt(orders[a], 10)
  );
};
