# HESTIA JSON Schema

HESTIA JSON Schema definitions, used to validate JSON files following hestia schmea.

## Install

```sh
npm install @hestia-earth/json-schema
```
