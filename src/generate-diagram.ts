import { writeFileSync } from 'fs';
import { join } from 'path';

import { ensureDir, IClass, listYmlDir, readYmlFile } from './generate-utils';
import { ignoreClass, generateClassDiagram, getRelationships } from './diagram-utils';

const OUTPUT_DIR = join(__dirname, '..', 'diagrams');
const outputFile = join(OUTPUT_DIR, 'schema.mmd');

const generate = (classes: IClass[]) => {
  const ignoreClasses = ignoreClass(classes);
  const filteredClasses = classes.filter(({ name }) => !ignoreClasses(name));
  const relationships = getRelationships(filteredClasses);
  const content = generateClassDiagram(filteredClasses, relationships);
  writeFileSync(outputFile, content);
};

const run = () => {
  ensureDir(OUTPUT_DIR);
  generate(listYmlDir().map(readYmlFile));
};

run();
