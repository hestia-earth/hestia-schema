import { readdirSync, readFileSync, mkdirSync } from 'fs';
import { join } from 'path';
import { parse } from 'yamljs';

export const VERSION = require(join(__dirname, '..', 'package.json')).version;

export const YAML_DIR = join(__dirname, '..', 'yaml');

export const ensureDir = (dir: string) => {
  try {
    mkdirSync(dir, { recursive: true });
  } catch (err) {}
};

export const readYmlFile = (file: string) => parse(readFileSync(join(YAML_DIR, file), 'utf8')).class as IClass;

export const listYmlDir = () => readdirSync(YAML_DIR);

export const unique = <T>(values: T[]): T[] => [...new Set(values)];

export interface IClass {
  name: string;
  type: 'Node' | 'Blank Node';
  examples?: string[];
  doc: string;
  properties: IProperty[];
  validation?: any;
}

export enum PropertyType {
  array = 'array',
  number = 'number',
  integer = 'integer',
  string = 'string',
  boolean = 'boolean',
  date = 'date',
  'date-time' = 'date-time',
  GeoJSON = 'GeoJSON',
  iri = 'iri',
  null = 'null'
}

export interface IProperty {
  name: string;
  type: PropertyType;
  doc?: string;
  enum?: string[];
  const?: any;
  pattern?: string;
  // default value if none is provided
  default?: any;
  unique?: boolean;
  required?: boolean;
  internal?: boolean;
  searchable?: boolean;
  aggregatedQualityScoreIncluded?: boolean;
  schemaMapping?: {
    [field: string]: string[];
  }[];
  uniqueArrayItem?: string[];
}

export interface ISortedProperties {
  [key: string]: {
    order: string;
    type: string;
  };
}

const typeToClass: {
  [type in PropertyType]: string;
} = {
  [PropertyType.array]: 'any[]',
  [PropertyType.number]: 'number',
  [PropertyType.integer]: 'number',
  [PropertyType.string]: 'string',
  [PropertyType.boolean]: 'boolean',
  [PropertyType.date]: 'Date',
  [PropertyType['date-time']]: 'Date',
  [PropertyType.GeoJSON]: 'any',
  [PropertyType.iri]: 'any',
  [PropertyType.null]: 'any'
};

export const isNode = (type: string) => type.includes('Ref');

export const asJDONLDType = (type: string) => {
  const atype = cleanType(type);
  return isNode(type) ? (atype.endsWith('[]') ? `I${atype.replace('[]', '')}JSONLD[]` : `I${atype}JSONLD`) : atype;
};

const isPivoted = (type: string) => type.includes('List') && !type.includes('Ref');

const toPivotedType = (type: string, isArray = false) =>
  `Pick<I${type.replace('[]', '')}Pivoted, '@type' | '@id' | 'name'>${isArray ? '[]' : ''}`;

const typeToPivotedValues = {
  Completeness: '{ [key: string]: boolean }',
  Bibliography: 'Bibliography'
};

export const asPivotedType = (type: string, asProperty = false) => {
  const atype = cleanType(type);
  const isArray = atype.endsWith('[]');
  return isNode(type)
    ? asProperty
      ? toPivotedType(atype, isArray)
      : `I${atype.replace('[]', '')}Pivoted${isArray ? '[]' : ''}`
    : isPivoted(type)
    ? typeToPivotedValues[atype] || '{ [key: string]: { value: number } }'
    : atype;
};

export const asType = (type: string) =>
  type
    .replace('JSONLD', '')
    .replace('Pivoted', '')
    .replace(/^([I])([A-Z])/, '$2');

export const isRef = (type: string) => type.includes('Embed') || type.includes('Ref') || type.includes('List');

export const isTypeArray = (type: string) => type.includes('List[') || type.includes('array[');

export const cleanType = (type: string, includeArrays = true) =>
  type
    .replace(/(Embed\[)([a-zA-Z]*)(\])/g, '$2')
    .replace(/(Ref\[)([a-zA-Z]*)(\])/g, '$2')
    .replace(/(List\[)([a-zA-Z]*)(\])/g, includeArrays ? '$2[]' : '$2')
    .replace(/(array\[)([a-zA-Z\|]*)(\])/g, includeArrays ? '($2)[]' : '$2');

export const findPropertyType = (type: string) => (type in typeToClass ? typeToClass[type] : type);

export const dependencies = ({ name, properties }: IClass, includeJSONLD = false, includePivoted = false) => [
  ...properties
    .map(({ type }) => findPropertyType(type))
    .filter(isRef)
    .map(type => cleanType(type).replace(/\[\]/g, ''))
    .filter(type => !(type in typeToClass) && type !== name),
  ...(includeJSONLD
    ? properties
        .map(({ type }) => findPropertyType(type))
        .filter(isNode)
        .map(type => asJDONLDType(type).replace(/\[\]/g, ''))
    : []),
  ...(includePivoted
    ? properties
        .map(({ type }) => findPropertyType(type))
        .filter(isNode)
        .map(type => asPivotedType(type).replace(/\[\]/g, ''))
    : [])
];

export const nestedKeys = ({ properties }: IClass) =>
  properties.filter(({ searchable, type }) => searchable && type.includes('List')).map(({ name }) => name);

export interface IValidation {
  paths: string[];
  values: string[];
}

// TODO: should handle any className, not just Term
export const validationClass = 'Term';
export const validationPropertyName = 'termType';
export const validationEnumName = 'TermTermType';

const validationEnums = ({ name, properties }) => {
  const propertyName = Object.keys(properties || {})[0];
  const property = propertyName ? properties[propertyName] : {};
  return 'items' in property
    ? validationEnums({ name, properties: property.items.properties })
    : 'enum' in property
    ? // only handle TermTermType for now
      propertyName === validationPropertyName
      ? { name, values: property.enum }
      : { values: [] }
    : 'properties' in property
    ? validationEnums({
        name: name === propertyName ? name : [name, propertyName].join('.'),
        properties: property.properties
      })
    : { values: [] };
};

export const getValidations = (validation: any): IValidation[] =>
  (validation.allOf || [])
    .filter(v => v.if?.required?.length === 1 && v.then?.properties)
    .map(v => ({ name: v.if.required[0], properties: v.then.properties }))
    .filter(({ name, properties }) => name in properties)
    .map(validationEnums)
    .filter(({ values }) => values.length)
    .map(
      ({ name, values }) =>
        ({
          paths: name.split('.'),
          values: values.map(v => `${validationEnumName}.${v}`)
        }) as IValidation
    );

const defaultProperties: IProperty[] = [
  {
    name: 'type',
    type: PropertyType.string
  },
  {
    name: '@type',
    type: PropertyType.string
  },
  {
    name: '@id',
    type: PropertyType.string
  },
  {
    name: 'id',
    type: PropertyType.string
  }
];

export const capitalize = (value: string) => `${value.charAt(0).toUpperCase()}${value.substring(1)}`;

const enumName = (...parts: string[]) =>
  parts
    .flat()
    .filter(Boolean)
    .map(part => `${part.charAt(0).toUpperCase()}${part.substring(1)}`)
    .join('');

export const propertyIsEnum = (property: Partial<IProperty>) =>
  !!property.enum && property.enum.every(val => typeof val === 'string');

export const propertyEnumName = (className: string, { name }: Partial<IProperty>) => enumName(className, name);

export const propertyHasField = (field: keyof IProperty) => (property: IProperty) => field in property;

const propertyEnum = (className: string, property: Partial<IProperty>) =>
  propertyIsEnum(property)
    ? propertyEnumName(className, property)
    : property.enum.map(t => (property.type === PropertyType.string ? `'${t}'` : t)).join('|');

export const propertyType = (prop: IProperty, className?: string, isJSONLD = false, isPivoted = false) =>
  prop.enum && className
    ? propertyEnum(className, prop)
    : isJSONLD
    ? asJDONLDType(findPropertyType(prop.type))
    : isPivoted
    ? asPivotedType(findPropertyType(prop.type), true)
    : cleanType(findPropertyType(prop.type));

export const sortedProperties = (properties: IProperty[]): ISortedProperties =>
  [...defaultProperties, ...properties].reduce(
    (prev, prop, index) => ({
      ...prev,
      [prop.name]: {
        order: index.toString().padStart(properties.length.toString().length, '0'),
        type: propertyType(prop).replace(/\[\]/g, '')
      }
    }),
    {}
  );

export const getSortConfig = classes =>
  classes.reduce(
    (prev, { name, sortedProperties }, index) => ({
      ...prev,
      [name]: {
        index: {
          order: index.toString().padStart(2, '0'),
          type: 'string'
        },
        ...sortedProperties
      }
    }),
    {}
  );

export const generateCompletenessMapping = (properties: IProperty[]) =>
  properties
    .filter(({ schemaMapping }) => schemaMapping?.length > 0)
    .flatMap(({ name, schemaMapping }) =>
      schemaMapping.flatMap(mapping =>
        Object.entries(mapping)
          .filter(([key]) => key.endsWith('term.termType'))
          .flatMap(([key, termTypes]) => ({
            type: capitalize(key.split('.')[0]),
            termTypes: Object.fromEntries(termTypes.map(termType => [termType, name]))
          }))
      )
    )
    .reduce((p, c) => ({ ...p, [c.type]: { ...(p[c.type] || {}), ...c.termTypes } }), {});
