import { IClass, IProperty, cleanType, isRef, isTypeArray, findPropertyType, unique } from './generate-utils';

const [skipBlankNodes] = process.argv.slice(2);

export interface IRelationship {
  className: string;
  type: string;
  isArray: boolean;
  properties: string[];
}

const ignoredClasses = ['Actor'];
export const ignoreClass = (classes: IClass[]) => {
  const nodes = classes.filter(({ type }) => type === 'Node').map(({ name }) => name);

  return (name: string) => ignoredClasses.includes(name) || (skipBlankNodes === 'true' && !nodes.includes(name));
};

const SPACER = ' '.repeat(2);
const includeRelationshipFields = false;

const cleanPropertyType = (type: string) =>
  findPropertyType(type)
    .replace(/(Embed\[)([a-zA-Z]*)(\])/g, '$2')
    .replace(/(Ref\[)([a-zA-Z]*)(\])/g, '$2')
    .replace(/(array\[)([a-zA-Z\|]*)(\])/g, 'List[$2]')
    .replace(/([A-Za-z]+)[\|]([A-Za-z]+)/, '$1')
    .replace(/([A-Za-z]+)[\|]([A-Za-z]+)[\|]([A-Za-z]+)/, '$1')
    .replace(/([A-Za-z]+)[\|]([A-Za-z]+)[\|]([A-Za-z]+)[\|]([A-Za-z]+)/, '$1')
    .replace('any', 'Object')
    .replace('boolean', 'Bool')
    .replace('string', 'String')
    .replace('number', 'Number');

const sortByType = (a: IProperty, b: IProperty) => cleanPropertyType(a.type).localeCompare(cleanPropertyType(b.type));

const sortByRef = (a: IProperty, b: IProperty) => (isRef(a.type) ? 1 : isRef(b.type) ? -1 : 0);

export const getRelationships = (classes: IClass[]) =>
  unique(
    Object.entries(
      classes
        .flatMap(({ name, properties }) =>
          properties
            .filter(({ type }) => isRef(type))
            .map(property => ({
              name: property.name,
              type: cleanType(property.type, false),
              className: name,
              isArray: isTypeArray(property.type)
            }))
        )
        .reduce((prev, { className, type, isArray, name }) => {
          prev[className] = prev[className] || {};
          // arrays have higher importance
          prev[className][type] = prev[className][type] || { properties: [] };
          prev[className][type].properties.push(name);
          prev[className][type].isArray = prev[className][type].isArray || isArray;
          return prev;
        }, {})
    )
      .flatMap(([className, mapping]) =>
        Object.entries(mapping).map(
          ([type, { properties, isArray }]) =>
            ({
              className,
              type,
              isArray,
              properties
            }) as IRelationship
        )
      )
      .filter(({ className, type }) => className !== type && !ignoreClass(classes)(type))
  );

export const generateClassDiagram = (classes: IClass[], relationships: IRelationship[], includeProperties = false) =>
  `
classDiagram

${classes
  .map(({ name, properties }) =>
    `
class ${name} ${
      includeProperties
        ? `{\n${SPACER}` +
          properties
            .filter(({ internal }) => !internal)
            .sort(sortByType)
            .sort(sortByRef)
            .map(property =>
              `
  +${cleanPropertyType(property.type)} ${property.name}
    `.trim()
            )
            .join(`\n${SPACER}`) +
          '\n}'
        : ''
    }
      `.trim()
  )
  .join('\n\n')}

${relationships
  .map(({ className, type, isArray, properties }) =>
    `
${className} "1" --> "${isArray ? '0..*' : '1'}" ${type}${
      includeRelationshipFields ? `: fields=${properties.join(', ')}` : ''
    }
    `.trim()
  )
  .filter(Boolean)
  .join('\n\n')}
`.trim();
