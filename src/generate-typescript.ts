import { writeFileSync } from 'fs';
import { join } from 'path';

import {
  VERSION,
  ensureDir,
  unique,
  readYmlFile,
  listYmlDir,
  IProperty,
  dependencies,
  nestedKeys,
  IValidation,
  validationClass,
  validationEnumName,
  getValidations,
  getSortConfig,
  sortedProperties,
  ISortedProperties,
  propertyEnumName,
  propertyIsEnum,
  propertyType,
  asType,
  propertyHasField
} from './generate-utils';

const newLineJoin = ',\n  ';
interface IGeneratedClass {
  name: string;
  isNode: boolean;
  isBlankNode: boolean;
  properties: IProperty[];
  searchable: string[];
  sortedProperties: ISortedProperties;
  nestedKeys: string[];
  validations: IValidation[];
}

const CLASS_DIR = join(__dirname, '@hestia-earth', 'schema');
const SORT_CONFIG_DIR = join(CLASS_DIR, 'sort-config');

const cleanPropertyName = (name: string) => (name.includes('-') || name.includes(' ') ? `'${name}'` : name);

const variableName = (...parts: string[]) =>
  parts
    .flat()
    .filter(Boolean)
    .map((part, index) =>
      index === 0
        ? `${part.charAt(0).toLowerCase()}${part.substring(1)}`
        : `${part.charAt(0).toUpperCase()}${part.substring(1)}`
    )
    .join('');

const generateProperty =
  (className: string, isClass = true, isJSONLD = false, isPivoted = false) =>
  ({ doc, const: constValue, ...prop }: IProperty) =>
    `${
      doc
        ? `/**
  * ${doc.trim()}
  */`
        : ''
    }
  ${cleanPropertyName(prop.name)}?: ${propertyType(prop, className, isJSONLD, isPivoted)}${
    isClass && constValue ? ` = ${constValue}` : ''
  };
`;

interface IGenerateContent {
  className: string;
  doc: string;
  properties: IProperty[];
}

const JSONContent = ({ className, doc, properties }: IGenerateContent) => `
/**
 * ${doc.trim()}
 */
export class ${className} extends JSON<SchemaType.${className}> {
  ${properties.map(generateProperty(className)).join('\n  ')}
}`;

const JSONLDContent = ({ className, doc, properties }: IGenerateContent) => `
/**
 * ${doc.trim()}
 */
export interface I${className}JSONLD extends JSONLD<NodeType.${className}> {
  ${properties
    .filter(({ name }) => name !== 'name')
    .map(generateProperty(className, false, true))
    .join('\n  ')}
}`;

const pivotedContent = ({ className, doc, properties }: IGenerateContent) => `
/**
 * ${doc.trim()}
 */
export interface I${className}Pivoted extends JSONLD<NodeType.${className}> {
  ${properties
    .filter(({ name }) => name !== 'name')
    .map(generateProperty(className, false, false, true))
    .join('\n  ')}
}
`;

const generateEnums = (className: string, properties: IProperty[]) =>
  properties
    .filter(propertyIsEnum)
    .map(
      property => `
export enum ${propertyEnumName(className, property)} {
  ${property.enum
    .sort()
    .map(val => `${cleanPropertyName(val)} = '${val}'`)
    .join(newLineJoin)}
}`
    )
    .join('\n\n');

const generateValidations = (className: string, validations: IValidation[]) => `
/**
 * Contains all the ${validationEnumName} with override on the ${className}.
 * Note: it does not contain the default ${validationEnumName} on related Blank Nodes.
 */
export const ${variableName(className, validationEnumName)} = {
  ${validations
    .map(({ paths, values }) =>
      `
    ${paths.join(': {')}: [
    ${values.join(newLineJoin)}
  ]${paths.map(_v => '').join(' }')}
    `.trim()
    )
    .join(newLineJoin)},
  all: [
    ${unique(validations.flatMap(({ values }) => values)).join(newLineJoin)}
  ]
};`;

const uniquenessFields = (properties: IProperty[]) =>
  properties
    .filter(propertyHasField('uniqueArrayItem'))
    .map(({ name, uniqueArrayItem }) => `${name}: [${uniqueArrayItem.map(name => `'${name}'`).join(newLineJoin)}]`)
    .join(newLineJoin);

const generateUniquenessFields = (className: string, properties: IProperty[]) =>
  properties.some(propertyHasField('uniqueArrayItem'))
    ? `
/**
 * Contains all the fields that make the blank nodes unique.
 */
export const ${variableName(className, 'UniquenessFields')} = {
  ${uniquenessFields(properties)}
};`
    : '';

export const generateAllFields = (className: string, properties: IProperty[]) =>
  `
export const ${variableName(className, 'Fields')} = [
  ${properties.map(({ name }) => `'${name}'`).join(newLineJoin)}
];
`;

const toTypeList = (classes: IGeneratedClass[]) => classes.map(({ name }) => `| ${name}.${name}`).join('\n  ');

const generateClass = (file: string): IGeneratedClass => {
  console.log(`Processing file: ${file}`);
  const data = readYmlFile(file);
  const isNode = data.type === 'Node';
  const properties: IProperty[] = data.properties;
  const importedClasses = dependencies(data, isNode, isNode);
  const validations = data.validation ? getValidations(data.validation) : [];
  const content = `// auto-generated content
  import { JSON, ${isNode ? 'JSONLD, NodeType, ' : ''}SchemaType } from './types';
${unique(importedClasses)
  .map(name => [name, asType(name)])
  .filter(([name, type]) => name !== data.name && type !== data.name)
  .map(([name, type]) => `import { ${name} } from './${type}';`)
  .join('\n')}
${validations.length ? `import { ${validationEnumName} } from './${validationClass}';` : ''}
${generateEnums(data.name, properties)}
${validations.length ? generateValidations(data.name, validations) : ''}
${generateUniquenessFields(data.name, properties)}
${JSONContent({ className: data.name, doc: data.doc, properties })}
${isNode ? JSONLDContent({ className: data.name, doc: data.doc, properties }) : ''}
${isNode ? pivotedContent({ className: data.name, doc: data.doc, properties }) : ''}
${isNode ? generateAllFields(data.name, properties) : ''}`;
  writeFileSync(join(CLASS_DIR, `${data.name}.ts`), content);
  return {
    name: data.name,
    isNode,
    isBlankNode: properties.some(prop => prop.name === 'term'),
    properties,
    searchable: properties.filter(prop => prop.searchable).map(prop => prop.name),
    sortedProperties: sortedProperties(properties),
    nestedKeys: nestedKeys(data),
    validations
  };
};

const generateTypes = (classes: IGeneratedClass[]) => {
  const content = `// auto-generated content

export enum NodeType {
  ${classes
    .filter(({ isNode }) => isNode)
    .map(({ name }) => `${name} = '${name}'`)
    .join(newLineJoin)}
}

export enum SchemaType {
  ${classes.map(({ name }) => `${name} = '${name}'`).join(newLineJoin)}
}

export const searchableProperties: {
  [type in SchemaType]: string[];
} = {
  ${classes.map(({ name, searchable }) => `${name}: [${searchable.map(v => `'${v}'`).join(', ')}]`).join(newLineJoin)}
};

export const nestedSearchableKeys = [
  ${unique(classes.flatMap(({ nestedKeys }) => nestedKeys))
    .map(v => `'${v}'`)
    .join(', ')}
];

export const uniquenessFields = {
  ${classes
    .filter(({ properties }) => properties.some(propertyHasField('uniqueArrayItem')))
    .map(({ name, properties }) => `${name}: {${uniquenessFields(properties)}}`)
    .join(newLineJoin)}
};

export class JSON<T extends SchemaType> {
  type: T;
  /**
   * Not required, used to generate contextualized unique id.
   */
  id?: string;
}

export interface IContext {
  '@base': string;
  '@vocab': string;
}

export interface JSONLD<T extends NodeType> {
  '@context': string|Array<string|IContext>;
  '@type': T;
  '@id': string;
  name: string;
}
`;
  writeFileSync(join(CLASS_DIR, 'types.ts'), content, 'utf8');

  const sortConfig = getSortConfig(classes);
  writeFileSync(join(SORT_CONFIG_DIR, 'config.json'), JSON.stringify(sortConfig), 'utf8');
};

const generateTermTypes = (classes: IGeneratedClass[]) => {
  const nodes = classes.filter(({ isNode }) => isNode);
  const blankNodes = classes.filter(({ isNode, isBlankNode }) => !isNode && isBlankNode);
  const blankNodesKeys = unique(
    nodes
      .flatMap(({ properties }) => properties)
      .filter(prop => blankNodes.some(({ name }) => prop.type.includes(name)))
      .map(({ name }) => name)
      .sort()
  );
  const nonBlankNodes = classes.filter(({ isNode, isBlankNode }) => !isNode && !isBlankNode);
  const nonBlankNodesKeys = unique(
    nodes
      .flatMap(({ properties }) => properties)
      .filter(prop => nonBlankNodes.some(({ name }) => prop.type.includes(name)))
      .map(({ name }) => name)
      .sort()
  );
  const content = `// auto-generated content

import { SchemaType } from './types';

${classes.map(({ name }) => `import * as ${name} from './${name}';`).join('\n')}

export type nodesType =
  ${toTypeList(nodes)};

export type blankNodesType =
  ${toTypeList(blankNodes)};

export type blankNodesKey =
  ${blankNodesKeys.map(key => `| '${key}'`).join('\n  ')}

export enum BlankNodesKey {
  ${blankNodesKeys.map(name => `${name} = '${name}'`).join(newLineJoin)}
}

export type nonBlankNodesType =
  ${toTypeList(nonBlankNodes)};

export type nonBlankNodesKey =
  ${nonBlankNodesKeys.map(key => `| '${key}'`).join('\n  ')}

export enum NonBlankNodesKey {
  ${nonBlankNodesKeys.map(name => `${name} = '${name}'`).join(newLineJoin)}
}

export const schemaTermTypes = [
  ${classes
    .filter(({ validations }) => validations.length)
    .map(({ name }) => `{ type: SchemaType.${name}, mappings: ${name}.${variableName(name, validationEnumName)} }`)
    .join(newLineJoin)}
];
`;
  writeFileSync(join(CLASS_DIR, 'termTypes.ts'), content, 'utf8');
};

const generateIndex = (classes: string[]) => {
  const content = `// auto-generated content

export const SCHEMA_VERSION = '${VERSION}';
export * from './types';
export * from './termTypes';
export * from './utils/sort';
export * from './utils/types';

${classes.map(name => `export * from './${name}';`).join('\n')}
`;
  writeFileSync(join(CLASS_DIR, 'index.ts'), content);
};

const run = () => {
  ensureDir(CLASS_DIR);
  ensureDir(SORT_CONFIG_DIR);
  const files = listYmlDir();
  const classes = files.map(generateClass);
  generateTypes(classes);
  generateTermTypes(classes);
  generateIndex(classes.map(({ name }) => name));
};

run();
