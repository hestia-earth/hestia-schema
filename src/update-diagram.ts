import { writeFileSync } from 'fs';
import { join } from 'path';

import { IClass, listYmlDir, readYmlFile } from './generate-utils';
import { ignoreClass, generateClassDiagram, getRelationships } from './diagram-utils';

const OUTPUT_DIR = join(__dirname, '..');
const outputFile = join(OUTPUT_DIR, 'schema.md');

const update = (classes: IClass[]) => {
  const ignoreClasses = ignoreClass(classes);
  const filteredClasses = classes.filter(({ name }) => !ignoreClasses(name));
  const relationships = getRelationships(filteredClasses);
  const content = `# Class Diagram

> This is a reprensentation of the Schema as a Class diagram.
It uses [mermaidjs](https://mermaid.js.org/syntax/classDiagram.html) for representation.

\`\`\`mermaid
${generateClassDiagram(filteredClasses, relationships, true)}
\`\`\`
  `;
  writeFileSync(outputFile, content);
};

const run = () => {
  update(listYmlDir().map(readYmlFile));
};

run();
